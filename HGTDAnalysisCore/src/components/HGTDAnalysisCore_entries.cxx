
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"

DECLARE_ALGORITHM_FACTORY( HGTDAnalysisAlgBase )

//
// #include "HGTDAnalysisCore/JetControlPlots.h"
// DECLARE_ALGORITHM_FACTORY( JetControlPlots )


// #include "HGTDAnalysisCore/TrackTimeControlPlots.h"
// DECLARE_ALGORITHM_FACTORY( TrackTimeControlPlots )


// #include "HGTDAnalysisCore/PileupRejection.h"
// DECLARE_ALGORITHM_FACTORY( PileupRejection )

#include "HGTDAnalysisCore/PileupRejectionNtuple.h"
DECLARE_ALGORITHM_FACTORY( PileupRejectionNtuple )

#include "HGTDAnalysisCore/HitControlPlots.h"
DECLARE_ALGORITHM_FACTORY( HitControlPlots )

#include "HGTDAnalysisCore/AlexNtuplizer.h"
DECLARE_ALGORITHM_FACTORY( HGTDHitAnalysis )


#include "HGTDAnalysisCore/NtupleProduction.h"
DECLARE_ALGORITHM_FACTORY( NtupleProduction )


// #include "HGTDAnalysisCore/VertexControlPlots.h"
// DECLARE_ALGORITHM_FACTORY( VertexControlPlots )

DECLARE_FACTORY_ENTRIES( HGTDAnalysisCore )
{
  // DECLARE_ALGORITHM( VertexControlPlots );
  DECLARE_ALGORITHM( NtupleProduction );
  DECLARE_ALGORITHM( HGTDHitAnalysis );
  DECLARE_ALGORITHM( HitControlPlots );
  // DECLARE_ALGORITHM( PileupRejection );
  DECLARE_ALGORITHM( PileupRejectionNtuple );
  // DECLARE_ALGORITHM( TrackTimeControlPlots );
  // DECLARE_ALGORITHM( JetControlPlots );
  DECLARE_ALGORITHM( HGTDAnalysisAlgBase );
}
