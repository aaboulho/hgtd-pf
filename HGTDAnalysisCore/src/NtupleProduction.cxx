// HGTDAnalysisCore includes
#include "HGTDAnalysisCore/NtupleProduction.h"

#include "EventInfo/EventInfo.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "InDetRIO_OnTrack/PixelClusterOnTrack.h"
#include "InDetRawData/PixelRDO_Container.h"
#include "InDetReadoutGeometry/PixelModuleDesign.h"
#include "InDetReadoutGeometry/SiDetectorElement.h"
#include "InDetSimData/InDetSimDataCollection.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthVertex.h"


//#include "eflowRec/eflowEEtaBinnedParameters.h"
#include <iostream>
#include <utility>



NtupleProduction::NtupleProduction(const std::string &name,
                                   ISvcLocator *pSvcLocator)
    : HGTDAnalysisAlgBase(name, pSvcLocator), m_histSvc("THistSvc", name),
      m_jet_min_pt_cut(20.), m_track_min_pt_cut(1.0), m_include_hits(true) {
  declareProperty("JetMinPtCut", m_jet_min_pt_cut);
  declareProperty("TrackMinPtCut", m_track_min_pt_cut);
  declareProperty("IncludeHits", m_include_hits);
}



NtupleProduction::~NtupleProduction() {}

StatusCode NtupleProduction::initialize() {
  ATH_CHECK(HGTDAnalysisAlgBase::initialize());
  ATH_MSG_INFO("Initializing " << name() << "...");

  ATH_CHECK(m_histSvc.retrieve());

//surface object  
  ATH_CHECK(m_extrapolator.retrieve());
  setupSurfacesForExtrapolation();
  ATH_CHECK(m_magFieldSvc.retrieve());
//  ATH_CHECK(m_histSvc.retrieve());   

  m_jet_tree = new TTree("Jets", "NtupleCollection");
  CHECK(m_histSvc->regTree("/NTUPLE/Jets", m_jet_tree));

  m_jet_tree->Branch("m_run_number", &m_run_number);
  m_jet_tree->Branch("m_event_number", &m_event_number);
  m_jet_tree->Branch("m_jet_px", &m_jet_px);
  m_jet_tree->Branch("m_jet_e", &m_jet_e);
  m_jet_tree->Branch("m_jet_pt", &m_jet_pt);
  m_jet_tree->Branch("m_jet_eta", &m_jet_eta);
  m_jet_tree->Branch("m_jet_phi", &m_jet_phi);
  m_jet_tree->Branch("m_jet_mass", &m_jet_mass);
  m_jet_tree->Branch("m_jet_class", &m_jet_class);
  m_jet_tree->Branch("m_jet_pt_corr", &m_jet_pt_corr);
  m_jet_tree->Branch("m_track_sump", &m_track_sump);
  m_jet_tree->Branch("m_track_sumpt", &m_track_sumpt);

  m_jet_tree->Branch("m_DeltaRPV", &m_DeltaRPV);
  m_jet_tree->Branch("m_DeltaREM1", &m_DeltaREM1);
  m_jet_tree->Branch("m_DeltaR", &m_DeltaR);
  m_jet_tree->Branch("m_tree_density", &m_tree_density);


  m_jet_tree->Branch("m_nmb_PUtrack", &m_nmb_PUtrack);
  m_jet_tree->Branch("m_HStrack_sumpt", &m_HStrack_sumpt);
  m_jet_tree->Branch("m_nmb_HStrack", &m_nmb_HStrack);
  m_jet_tree->Branch("m_nmb_track", &m_nmb_track);
  m_jet_tree->Branch("m_nmb_TruthP", &m_nmb_TruthP);
  m_jet_tree->Branch("m_nmb_TruthPHS", &m_nmb_TruthPHS);
  m_jet_tree->Branch("m_nmb_TruthPPU", &m_nmb_TruthPPU);


  m_jet_tree->Branch("m_truth_jet_e", &m_truth_jet_e);
  m_jet_tree->Branch("m_truth_jet_pt", &m_truth_jet_pt);
  m_jet_tree->Branch("m_truth_jet_eta", &m_truth_jet_eta);
  m_jet_tree->Branch("m_truth_jet_phi", &m_truth_jet_phi);

  m_jet_tree->Branch("m_track_pt", &m_track_pt);
  m_jet_tree->Branch("m_track_eta", &m_track_eta);
  m_jet_tree->Branch("m_track_phi", &m_track_phi);
  m_jet_tree->Branch("m_track_p", &m_track_p);
  m_jet_tree->Branch("delta_t", &delta_t);
  m_jet_tree->Branch("track_time",&track_time);
  m_jet_tree->Branch("m_HStrack_pt", &m_HStrack_pt);
//  m_jet_tree->Branch("vertex_time", &vertex_time);
//  m_track_tree->Branch("m_PU-vertex", &m_PU_vertex);

  m_jet_tree->Branch("m_magFieldSvc1", &m_magFieldSvc1);
  m_jet_tree->Branch("m_magFieldSvc2", &m_magFieldSvc2);

 /* plot_name = "m_hist_n_hs_tracks_in_hgtd";
  m_hist_n_hs_tracks_in_hgtd = new TH1F(plot_name.c_str(), ";n HS tracks in HGTD; a.u.", 50, 0, 50);
  ATH_CHECK( m_histSvc->regHist(directory_name+plot_name, m_hist_n_hs_tracks_in_hgtd));

  plot_name = "m_hist_n_pu_tracks_in_hgtd";
  m_hist_n_pu_tracks_in_hgtd = new TH1F(plot_name.c_str(), ";n PU tracks in HGTD; a.u.", 70, 0, 700);
  ATH_CHECK( m_histSvc->regHist(directory_name+plot_name, m_hist_n_pu_tracks_in_hgtd));
*/

  m_jet_calibration_tool.setTypeAndName("JetCalibrationTool/myJESTool");
  if (m_jet_calibration_tool.retrieve().isFailure()) {
    ATH_MSG_ERROR("Failed retrieving JetCalibrationTool ...");
    return StatusCode::FAILURE;
  }

  if (m_include_hits) {
    if (detStore()->retrieve(m_pixel_ID, "PixelID").isFailure()) {
      return StatusCode::FAILURE;
    }

    if (detStore()->retrieve(m_pixel_manager, "Pixel").isFailure()) {
      return StatusCode::FAILURE;
    }
  }

  AccessorFactory accessor_factory;
  m_time_accessor =
  accessor_factory.createAccessor("TTA01_1_1_2.0_1.5_1_3.5_3.9_0");

  return StatusCode::SUCCESS;
}

StatusCode NtupleProduction::finalize() {
  ATH_CHECK(HGTDAnalysisAlgBase::finalize());
  ATH_MSG_INFO("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode NtupleProduction::execute() {
  ATH_CHECK(HGTDAnalysisAlgBase::execute());
  ATH_MSG_DEBUG("Executing " << name() << "...");

  // get the truth particles (HS and PU separately)
  auto truth_hs_event = m_truth_event_container->at(0);
  // auto truth_hs_vtx = truth_hs_event->truthVertex(1);
  auto hs_truthparticles = getTruthParticles(truth_hs_event, 1000.);

  std::vector<const xAOD::TruthParticle *> pu_truthparticles;
  for (size_t pu_event = 0; pu_event < m_pileup_truth_container->size();
       pu_event++) {
    auto truth_pu_event = m_pileup_truth_container->at(pu_event);
    auto pu_truthparticle_part = getTruthParticles(truth_pu_event, 1000.);
    pu_truthparticles.insert(pu_truthparticles.end(),
                             pu_truthparticle_part.begin(),
                             pu_truthparticle_part.end());
                    }
    //////////////////////////////
   //      jet Collection      //
  //////////////////////////////
  
  ATH_CHECK(HGTDAnalysisAlgBase::execute());
   ATH_MSG_DEBUG ("Executing " << name() << "...");
	
   const xAOD::JetContainer * jet_container = nullptr;
   if ( not evtStore()->retrieve(jet_container,"AntiKt4EMTopoJets").isSuccess() ) {
     ATH_MSG_ERROR ("Failed to retrieve AntiKt4EMTopoJets collection. ");
     return StatusCode::SUCCESS;
   }

   const xAOD::JetContainer* truth_jet_container = nullptr;
   if ( not evtStore()->retrieve(truth_jet_container,"AntiKt4TruthJets").isSuccess() ) {
     ATH_MSG_ERROR ("Failed to retrieve AntiKt4TruthJets collection. ");  
    return StatusCode::SUCCESS;
   }

   const xAOD::EventInfo* event_info = nullptr;
   if ( not evtStore()->retrieve(event_info,"EventInfo").isSuccess() ) {
     ATH_MSG_ERROR ("Failed to retrieve EventInfo collection. ");

  /*if ( evtStore()->retrieve(event_info,"EventInfo").isSuccess() ) {
     std::cout << "in execute, runNumber = " << event_info->runNumber() << ", eventNumber = " << event_info->eventNumber() << std::endl;
     return StatusCode::SUCCESS;}
  */

     return StatusCode::SUCCESS;
  }

  m_event_number = event_info->eventNumber();
  m_run_number = event_info->runNumber();
  auto reco_vx = getPrimaryRecoVertex();
  auto truth_vx = getTruthHSVertex();
  double vx_delta_z = fabs(reco_vx->z() - truth_vx->z());
  int  numJets=0 ;

  std::vector<xAOD::Jet*> passed_jets;
  for (auto* raw_jet : *jet_container) {
     xAOD::Jet *jet = nullptr;
     m_jet_calibration_tool->calibratedCopy(*raw_jet, jet);

  // size_t numberjet = jet->size();
  // std::cout <<"execute(): number of jets = " << numberjet <<std::endl;

     if(jet == nullptr) {continue;}
     m_jet_pt = jet->pt()/1.e3;

     if(m_jet_pt < 20) {
       delete jet;
       continue;
     }
 
    m_jet_e = jet->e() / 1.e3;
     m_jet_eta = jet->eta();
     m_jet_phi = jet->phi();
     // m_JetHist ->Fill(nbjet);
 
     double jet_rpt = jetRpT(jet, reco_vx);
     if (jet_rpt <0.5) {
       delete jet;
       continue;
      }


      numJets++;

//     std::cout << "Pt-reco = " <<m_jet_pt << std::endl;

     passed_jets.push_back(jet);
     int n_ghost_tracks = nGhostAssociatedTracks(jet, reco_vx);
     auto jet_cat = determineJetCategory(jet, truth_jet_container);
     if(jet_cat == JetCategory::HS) {
        float truth_jet_pt = 0.;
        for (const auto& truth_jet : *truth_jet_container) {
          if (truth_jet->pt()>30e3 && truth_jet->p4().DeltaR(jet->p4())<0.3) {
             m_truth_jet_pt = truth_jet->pt()/1.e3;
             m_truth_jet_e = truth_jet->e()/1.e3;
             m_truth_jet_eta = truth_jet->eta();
             m_truth_jet_phi= truth_jet->phi();

             //std::cout << "Pt-reco = " <<m_jet_pt << std::endl;
             //std::cout << "Pt-truth = " <<m_truth_jet_pt <<std::endl;

//     double jet_rsp=m_jet_pt/ m_truth_jet_pt;
//     if (jet_rsp <0.5){
//     std::cout << "jet-resp = " <<  jet_rsp<< std::endl;
//     }

//     m_jet_tree->Fill();
  //    	 }
     //  }
    // }
//m_tree_density = parametrisedLocalVertexDensity(reco_vx);



  //////////////////////////////
  //        T R A C K S       //
  //////////////////////////////



m_HStrack_pt.clear();
delta_t.clear();
m_track_pt.clear();
m_track_p.clear();
m_track_eta.clear();
m_track_phi.clear();
track_time.clear();
//vertex_time.clear();

double vertex_time= vertexTruthTime(truth_vx);
float  m_tracksump=0;
float  m_PUtracksumpt=0;
float  m_HStracksumpt=0;
float  m_nmbHStrack=0;
float  m_nmbPUtrack=0;
float  m_nmbtrack=0;
float  m_nmbTruthP=0;
float  m_nmbTruthPHS=0;
float  m_nmbTruthPPU=0;



double field[1];
double point[1];

double field2[1];
double point2[1];
//ITrackTimeAccessor* accessor;
std::vector<const xAOD::TrackParticle *> tracks; 



 auto jet_tracks = getGhostAssociatedTrackParticles(jet);
 for (const auto* track : jet_tracks) {
     const xAOD::TrackParticle* trk = static_cast<const xAOD::TrackParticle*>(track);
//	m_DeltaRPV = (jet->p4().DeltaR(trk->p4()));
        if (jet->p4().DeltaR(trk->p4()) < 0.4){
          m_DeltaRPV = (jet->p4().DeltaR(trk->p4()));          
	  point[0] = trk->z0();
	  m_magFieldSvc->getField(point,field);
	  m_magFieldSvc1 = m_magFieldSvc;
         // m_magFieldSvc.clean();

//if(trk->pt() > 1.e3) {continue;}
//    if(trk->pt() < 10e3) {continue;}
      if (passTrackVertexAssociation(trk,reco_vx)) {
//          m_nmbtrack = m_nmbtrack+1;

	TVector3 position(0,0,0); 
        TVector3 momentum(0,0,0);
        if (extrapolateTrackFromLastMeasurement(trk, position, momentum, 4)) {	

      int Zpos = ( (jet)->p4().Pz() > 0 ) ? 1 : ( ((jet)->p4().Pz() < 0) ? -1 : 0 ); // return the sign of Pz
      Float_t Xjet = tan(2*atan(exp(-1*(jet)->eta())))*(Zpos*m_hgtd_layer_z[4]-(truth_vx->z()))*cos((jet)->phi());
      Float_t Yjet = tan(2*atan(exp(-1*(jet)->eta())))*(Zpos*m_hgtd_layer_z[4]-(truth_vx->z()))*sin((jet)->phi());

      TVector3 pos;
      pos.SetXYZ(Xjet,Yjet,Zpos*m_hgtd_layer_z[4]);
	       
      m_DeltaREM1=(pos.DeltaR(position));
      m_DeltaR = fabs(m_DeltaRPV-m_DeltaREM1);

      point2[0] = position.Z();
          m_magFieldSvc->getField(point2,field2);
	  m_magFieldSvc2 = m_magFieldSvc;
	  //m_magFieldSvc=0;  

       auto truth_particle = getTruthParticle(trk);
       if (truth_particle) { m_nmbTruthP = m_nmbTruthP+1; } 
          m_nmbtrack = m_nmbtrack+1;
          m_tree_density = parametrisedLocalVertexDensity(reco_vx);


	  bool track_is_hs = false;
     	  auto truth_vertex = getTruthVertex(trk, track_is_hs);
          if (track_is_hs and truth_vertex) {
          if  (trk->pt()>1e3) {
	 //std::cout << "track is hs" <<std::endl;
          m_nmbHStrack = m_nmbHStrack+1;
	
       auto truth_particleHS = getTruthParticle(trk);
       if (truth_particleHS) { m_nmbTruthPHS = m_nmbTruthPHS+1; }

         // m_HStrack_pt.push_back(trk->pt()/1.e3);
          m_HStrack_pt.push_back(momentum.Perp());
	  m_HStracksumpt= m_HStracksumpt +(momentum.Perp());
         // std::cout << "pt-hs" <<m_HStrack_pt<<std::endl;
     	   }
	 }

     	 else if (not track_is_hs and truth_vertex) {     	
         if ( (trk->pt()>1e3) && (trk->pt()<10e3)){
	    m_track_pt.push_back(momentum.Perp());
	    m_PUtracksumpt = m_PUtracksumpt + (momentum.Perp());	            
            m_track_p.push_back(momentum.Mag());       // get magnitude (=rho=Sqrt(px*px+py*py+pz*pz))
   	    m_tracksump = m_tracksump + (momentum.Mag());
            m_track_eta.push_back(position.PseudoRapidity());
            m_track_phi.push_back(position.Phi());
            m_nmbPUtrack = m_nmbPUtrack+1;

       auto truth_particlePU = getTruthParticle(trk);
       if (truth_particlePU) { m_nmbTruthPPU = m_nmbTruthPPU+1; }

	   // m_track_tree->Fill();
		}
		 
	      }//end else 
 	     }//end of if  
	  }//end extrapolate     
        }//end association
      }//LOOP jet tracks
 m_track_sump = m_tracksump/1.e3;
 m_track_sumpt = m_PUtracksumpt/1.e3;
 m_HStrack_sumpt = m_HStracksumpt/1.e3;
 m_nmb_HStrack = m_nmbHStrack;
 m_nmb_PUtrack = m_nmbPUtrack;
 m_nmb_track = m_nmbtrack;
 m_nmb_TruthP = m_nmbTruthP;
 m_nmb_TruthPHS = m_nmbTruthPHS;
 m_nmb_TruthPPU = m_nmbTruthPPU;

 m_jet_pt_corr= m_jet_pt-(0.5*(m_track_sumpt));

 m_jet_tree->Fill();
  
 
    }
  }
}//end truth jet 


 delete jet;

}//end loop Jet 


return StatusCode::SUCCESS;

}
