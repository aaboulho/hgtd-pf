#ifndef XAOD_STANDALONE
// HGTDAnalysisCore includes
#include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"

#include "GaudiKernel/PhysicalConstants.h"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/PseudoJet.hh"

#include "TLorentzVector.h"



HGTDAnalysisAlgBase::HGTDAnalysisAlgBase(const std::string& name,
                                         ISvcLocator* pSvcLocator)
    : AthAlgorithm(name, pSvcLocator),
      m_extrapolator("Trk::Extrapolator/AtlasExtrapolator"),
      m_magFieldSvc("AtlasFieldSvc", name) {

  declareProperty("TimeRecoAlg", m_time_reco_alg);
  declareProperty("DirectoryName", m_directory_name);
  declareProperty("UseSurfaces", m_use_surfaces);
  declareProperty("UsePixelClusters", m_use_pixelclusters);
  declareProperty("UseExtrapolation", m_use_extrapolation);
  //declareProperty( "Property", m_nProperty ); //example property declaration
  declareProperty("Extrapolator", m_extrapolator);
//declareProperty("MagFieldSvc", m_magFieldSvc);
}


HGTDAnalysisAlgBase::~HGTDAnalysisAlgBase() {
  // for (size_t i=0; i<m_disc_positive_z.size(); i++) {
  //   if (m_disc_positive_z.at(i)) {
  //     delete m_disc_positive_z.at(i);
  //   }
  //   if (m_disc_negative_z.at(i)) {
  //     delete m_disc_negative_z.at(i);
  //   }
  // }
}

StatusCode HGTDAnalysisAlgBase::initialize() {
  ATH_MSG_INFO("Initializing HGTDAnalysisAlgBase ...");

  return StatusCode::SUCCESS;
}

StatusCode HGTDAnalysisAlgBase::finalize() {
  ATH_MSG_INFO("Finalizing HGTDAnalysisAlgBase ...");
  // for (size_t i=0; i<m_disc_positive_z.size(); i++) {
  //   if (m_disc_positive_z.at(i)) {
  //     delete m_disc_positive_z.at(i);
  //   }
  //   if (m_disc_negative_z.at(i)) {
  //     delete m_disc_negative_z.at(i);
  //   }
  // }
  return StatusCode::SUCCESS;
}
////////////////////////////////////////////////////////////////////////
///////////////////////    E X E C U T E    ////////////////////////////
////////////////////////////////////////////////////////////////////////

StatusCode HGTDAnalysisAlgBase::execute() {
  ATH_MSG_DEBUG("Executing HGTDAnalysisAlgBase ...");

  m_pileup_truth_container = nullptr;
  if (not evtStore()
              ->retrieve(m_pileup_truth_container, "TruthPileupEvents")
              .isSuccess()) {
    ATH_MSG_ERROR("Failed to retrieve TruthPileupEvents container. ");
    return StatusCode::SUCCESS;
  }

  m_truth_event_container = nullptr;
  if (not evtStore()
              ->retrieve(m_truth_event_container, "TruthEvents")
              .isSuccess()) {
    ATH_MSG_ERROR("Failed to retrieve TruthEvents container. ");
    return StatusCode::SUCCESS;
  }

  m_primary_vertex_container = nullptr;
  if (not evtStore()
              ->retrieve(m_primary_vertex_container, "PrimaryVertices")
              .isSuccess()) {
    ATH_MSG_ERROR("Failed to retrieve PrimaryVertices container. ");
    return StatusCode::SUCCESS;
  }

  m_track_container = nullptr;
  if (not evtStore()
              ->retrieve(m_track_container, "InDetTrackParticles")
              .isSuccess()) {
    ATH_MSG_ERROR("Failed to retrieve InDetTrackParticles collection. ");
    return StatusCode::SUCCESS;
  }

  return StatusCode::SUCCESS;
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

HGTDAnalysisAlgBase::JetCategory HGTDAnalysisAlgBase::determineJetCategory(
    const xAOD::Jet* jet, const xAOD::JetContainer* truth_jets) {
  bool is_hs = false;
  bool is_pu = true;
  for (const auto& tjet : *truth_jets) {
    if (tjet->pt() > 4e3 && tjet->p4().DeltaR(jet->p4()) < 0.6)
      is_pu = false;
    if (tjet->pt() > 10e3 && tjet->p4().DeltaR(jet->p4()) < 0.3)
      is_hs = true;
  }
  if (is_hs) {
    return JetCategory::HS;
  } else if (not is_hs and not is_pu) {
    return JetCategory::NEITHER;
  }
  // check if qcd or stoch
  bool is_qcd = false;
  bool is_stochastic = true;
  std::vector<std::vector<TLorentzVector>> truth_pu_jets = getTruthPuJets();

  for (size_t puVerti = 0; puVerti < truth_pu_jets.size(); puVerti++) {
    for (size_t puJeti = 0; puJeti < truth_pu_jets.at(puVerti).size();
         puJeti++) {
      if (truth_pu_jets.at(puVerti).at(puJeti).Pt() < 10e3) {
        continue;
      }
      if (truth_pu_jets.at(puVerti).at(puJeti).DeltaR(jet->p4()) < 0.6) {
        is_stochastic = false;
      }
      if (truth_pu_jets.at(puVerti).at(puJeti).DeltaR(jet->p4()) < 0.3) {
        is_qcd = true;
      }
    }
  }
  if (is_pu and is_qcd) {
    return JetCategory::QCD_PU;
  } else if (is_pu and is_stochastic) {
    return JetCategory::STOCH_PU;
  } else {
    return JetCategory::NEITHER_PU;
  }
}

std::vector<std::vector<TLorentzVector>> HGTDAnalysisAlgBase::getTruthPuJets() {

  std::vector<std::vector<TLorentzVector>> truth_pu_jets;

  fastjet::JetDefinition jet_def(fastjet::antikt_algorithm, 0.4);

  // ATH_MSG_DEBUG("HGTDAnalysisAlgBase::getTruthPuJets(): jet definitions
  // dones");

  for (const auto& tpe : *m_pileup_truth_container) {
    // ATH_MSG_DEBUG("HGTDAnalysisAlgBase::getTruthPuJets(): loop over PY
    // events");
    std::vector<TLorentzVector> buff;
    std::vector<fastjet::PseudoJet> input_particles;
    for (size_t i = 0; i < tpe->nTruthParticles(); i++) {
      const xAOD::TruthParticle* part =
          static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
      if (not part)
        continue;
      if (!includeParticle(part))
        continue;
      // ATH_MSG_DEBUG("HGTDAnalysisAlgBase::getTruthPuJets(): use this pseudo
      // jet");
      input_particles.push_back(fastjet::PseudoJet(
          part->p4().Px(), part->p4().Py(), part->p4().Pz(), part->e()));
    }
    // ATH_MSG_DEBUG("HGTDAnalysisAlgBase::getTruthPuJets(): input particles
    // done");
    fastjet::ClusterSequence clust_seq(input_particles, jet_def);
    std::vector<fastjet::PseudoJet> inclusive_jets =
        sorted_by_pt(clust_seq.inclusive_jets(0));

    // ATH_MSG_DEBUG("HGTDAnalysisAlgBase::getTruthPuJets(): cluster sequence
    // done");
    for (size_t i = 0; i < inclusive_jets.size(); i++) {
      TLorentzVector tlv;
      tlv.SetPtEtaPhiM(inclusive_jets.at(i).perp(), inclusive_jets.at(i).rap(),
                       inclusive_jets.at(i).phi(), 0);
      buff.push_back(tlv);
    }
    truth_pu_jets.push_back(buff);
  }
  return truth_pu_jets;
}

bool HGTDAnalysisAlgBase::includeParticle(const xAOD::TruthParticle* part) {
  if (part->status() != 1)
    return false;
  if (part->pdgId() == 21 && part->e() == 0)
    return false; // Work around for an old generator bug
  if (abs(part->pdgId()) == 13)
    return false; // muons
  if ((part->barcode()) > 2e5)
    return false; // from G4
  if (abs(part->pdgId()) == 12)
    return false; // neutrinos
  if (abs(part->pdgId()) == 14)
    return false; // neutrinos
  if (abs(part->pdgId()) == 16)
    return false; // neutrinos
  if (fabs(part->eta()) > 5)
    return false; // outside detector range
  // Assuming we are including W, Z, and taus
  return true;
}

std::vector<const xAOD::IParticle*>
HGTDAnalysisAlgBase::getGhostAssociatedTracks(xAOD::Jet* jet) {
  std::vector<const xAOD::IParticle*> jet_tracks;
  jet->getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack,
                                             jet_tracks);
  return jet_tracks;
}

std::vector<const xAOD::IParticle*>
HGTDAnalysisAlgBase::getGhostAssociatedTracks(const xAOD::Jet* jet) {
  std::vector<const xAOD::IParticle*> jet_tracks;
  jet->getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack,
                                             jet_tracks);
  return jet_tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::getGhostAssociatedTrackParticles(const xAOD::Jet* jet) {
  std::vector<const xAOD::TrackParticle*> jet_tracks;
  jet->getAssociatedObjects<xAOD::TrackParticle>(xAOD::JetAttribute::GhostTrack,
                                                 jet_tracks);
  return jet_tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::getDeltaRAssociatedTracks(xAOD::Jet* jet, double delta_r) {
  std::vector<const xAOD::TrackParticle*> tracks;
  auto jet_four_vec = jet->p4(); // TLorentzVector
  for (const auto& track : *m_track_container) {
    if (track->pt() < m_track_pt_cut) {
      continue;
    } // pT cut on tracks
    if (jet_four_vec.DeltaR(track->p4()) < delta_r) {
      tracks.push_back(track);
    }
  }
  return tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::getDeltaRAssociatedTracks(
    xAOD::Jet* jet, std::vector<const xAOD::TrackParticle*>& tracks,
    double delta_r) {
  std::vector<const xAOD::TrackParticle*> selected_tracks;
  auto jet_four_vec = jet->p4(); // TLorentzVector
  for (const auto& track : tracks) {
    if (track->pt() < m_track_pt_cut) {
      continue;
    } // pT cut on tracks
    if (jet_four_vec.DeltaR(track->p4()) < delta_r) {
      selected_tracks.push_back(track);
    }
  }
  return selected_tracks;
}

int HGTDAnalysisAlgBase::nGhostAssociatedTracks(xAOD::Jet* jet,
                                                const xAOD::Vertex* vx) {
  auto ghost_tracks = getGhostAssociatedTracks(jet);
  int n = 0;
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passTrackVertexAssociation(trk, vx)) {
      n++;
    }
  } // LOOP over tracks
  return n;
}

double HGTDAnalysisAlgBase::jetRpT(xAOD::Jet* jet, const xAOD::Vertex* vertex) {
  // get the ghost tracks
  auto ghost_tracks = getGhostAssociatedTracks(jet);
  double sum_trk_pt(0);
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passTrackVertexAssociation(trk, vertex)) {
      sum_trk_pt += trk->pt();
    }
  } // LOOP over tracks
  return sum_trk_pt / jet->pt();
}

double HGTDAnalysisAlgBase::jetRpT(xAOD::Jet* jet,
                                   const xAOD::TruthVertex* vertex) {
  // get the ghost tracks
  auto ghost_tracks = getGhostAssociatedTracks(jet);
  double sum_trk_pt(0);
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passTrackVertexAssociation(trk, vertex)) {
      sum_trk_pt += trk->pt();
    }
  } // LOOP over tracks
  return sum_trk_pt / jet->pt();
}

double HGTDAnalysisAlgBase::jetRpTHGTD(xAOD::Jet* jet,
                                       const xAOD::TruthVertex* vertex,
                                       bool has_time, float vertex_time,
                                       float vertex_time_res,
                                       ITrackTimeAccessor* accessor,
                                       float sigma_cut) {
  if (not has_time) { // if no t0 available, use standard RpT
    return jetRpT(jet, vertex);
  }
  // get the ghost tracks
  auto ghost_tracks = getGhostAssociatedTracks(jet);
  double sum_trk_pt(0);
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (not passTrackVertexAssociation(trk, vertex)) {
      continue;
    }
    if (accessor->hasTimeAssigned(*trk)) {
      float track_time = accessor->getTime(*trk);
      // |t - t_vx| < 2 sigma
      float delta_t = fabs(track_time - vertex_time);
      float track_res = accessor->getResolution(*trk);
      // float track_res =
      // m_hit_resolution/sqrt(accessor->numberAssignedHits(*trk)); //TODO
      // actual track time resolution
      float sigma =
          sqrt(vertex_time_res * vertex_time_res + track_res * track_res);
      if (delta_t / sigma > sigma_cut) {
        continue;
      }
      sum_trk_pt += trk->pt();
    } else {
      sum_trk_pt += trk->pt();
    }

  } // LOOP over tracks
  return sum_trk_pt / jet->pt();
}

double HGTDAnalysisAlgBase::jetRpTHGTD(xAOD::Jet* jet,
                                       const xAOD::Vertex* vertex,
                                       bool has_time, float vertex_time,
                                       float vertex_time_res,
                                       ITrackTimeAccessor* accessor,
                                       float sigma_cut) {
  if (not has_time) {
    return jetRpT(jet, vertex);
  }
  // get the ghost tracks
  auto ghost_tracks = getGhostAssociatedTracks(jet);
  double sum_trk_pt(0);
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (not passTrackVertexAssociation(trk, vertex)) {
      continue;
    }
    if (accessor->hasTimeAssigned(*trk)) {
      float track_time = accessor->getTime(*trk);
      // |t - t_vx| < 2 sigma
      float delta_t = fabs(track_time - vertex_time);
      float track_res = accessor->getResolution(*trk);
      // float track_res =
      // m_hit_resolution/sqrt(accessor->numberAssignedHits(*trk)); //TODO
      // actual track time resolution
      float sigma =
          sqrt(vertex_time_res * vertex_time_res + track_res * track_res);
      if (delta_t / sigma > sigma_cut) {
        continue;
      }
      sum_trk_pt += trk->pt();
    } else {
      sum_trk_pt += trk->pt();
    }
  } // LOOP over tracks
  return sum_trk_pt / jet->pt();
}

double HGTDAnalysisAlgBase::genericJetRpT(
    xAOD::Jet* jet, std::vector<const xAOD::TrackParticle*>& tracks) {
  // get the ghost tracks
  double sum_trk_pt(0);
  for (const auto* track : tracks) {
    if (track->pt() < m_track_pt_cut) {
      continue;
    }
    sum_trk_pt += track->pt();
  } // LOOP over tracks
  return sum_trk_pt / jet->pt();
}

double HGTDAnalysisAlgBase::subJetRpT(xAOD::Jet* jet,
                                      const xAOD::Vertex* vertex,
                                      ITrackTimeAccessor* accessor,
                                      double hit_time_resolution,
                                      CH::ClusterAlgoType algo,
                                      double cluster_distance) {

  auto ghost_tracks = getGhostAssociatedTracks(jet);

  ClusterCollection<const xAOD::TrackParticle*> collection;
  double no_time_sumpt = 0.;
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passTrackVertexAssociation(trk, vertex)) {
      if (accessor->hasTimeAssigned(*trk)) {
        double time = accessor->getTime(*trk);
        double resolution = accessor->getResolution(*trk);
        Cluster<const xAOD::TrackParticle*> cluster({time}, {resolution}, trk);
        collection.addCluster(cluster);
      } else {
        no_time_sumpt += track->pt();
      }
    }
  } // LOOP ghost tracks

  collection.updateDistanceCut(cluster_distance);
  collection.doClustering(algo);
  auto clusters = collection.getClusters();
  double max_sumpt = 0.;
  // Cluster max_sumpt_cluster;
  for (auto clus : clusters) {
    double cluster_sumpt = 0.;
    for (auto entrytrack : clus.getEntries()) {
      cluster_sumpt += entrytrack->pt();
    }
    if (cluster_sumpt > max_sumpt) {
      max_sumpt = cluster_sumpt;
    }
  }
  double sumpt = no_time_sumpt + max_sumpt;
  return sumpt / jet->pt();
}

double HGTDAnalysisAlgBase::subJetRpT(xAOD::Jet* jet,
                                      const xAOD::TruthVertex* vertex,
                                      ITrackTimeAccessor* accessor,
                                      double hit_time_resolution,
                                      CH::ClusterAlgoType algo,
                                      double cluster_distance) {

  auto ghost_tracks = getGhostAssociatedTracks(jet);

  ClusterCollection<const xAOD::TrackParticle*> collection;
  double no_time_sumpt = 0.;
  for (const auto* track : ghost_tracks) {
    const xAOD::TrackParticle* trk =
        static_cast<const xAOD::TrackParticle*>(track);
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passTrackVertexAssociation(trk, vertex)) {
      if (accessor->hasTimeAssigned(*trk)) {
        double time = accessor->getTime(*trk);
        double resolution = accessor->getResolution(*trk);
        Cluster<const xAOD::TrackParticle*> cluster({time}, {resolution}, trk);
        collection.addCluster(cluster);
      } else {
        no_time_sumpt += track->pt();
      }
    }
  } // LOOP ghost tracks

  collection.updateDistanceCut(cluster_distance);
  collection.doClustering(algo);
  auto clusters = collection.getClusters();
  double max_sumpt = 0.;
  // Cluster max_sumpt_cluster;
  for (auto clus : clusters) {
    double cluster_sumpt = 0.;
    for (auto entrytrack : clus.getEntries()) {
      cluster_sumpt += entrytrack->pt();
    }
    if (cluster_sumpt > max_sumpt) {
      max_sumpt = cluster_sumpt;
    }
  }
  double sumpt = no_time_sumpt + max_sumpt;
  return sumpt / jet->pt();
}

const xAOD::TruthParticle*
HGTDAnalysisAlgBase::getTruthParticle(const xAOD::TrackParticle* track) {
  if (track == nullptr) {
    return nullptr;
  }

  // using ElementLink<xAOD::TruthParticleContainer> = ElementTruthLink_t;
  using ElementTruthLink_t = ElementLink<xAOD::TruthParticleContainer>;
  const xAOD::TruthParticle* truth_particle(nullptr);
  if (not track->isAvailable<ElementTruthLink_t>("truthParticleLink")) {
    return nullptr;
  }
  const ElementTruthLink_t truth_link =
      track->auxdata<ElementTruthLink_t>("truthParticleLink");
  if (truth_link.isValid()) {
    truth_particle = *truth_link;
    return truth_particle;
  } else {
    return nullptr;
  }
}

const xAOD::TruthVertex*
HGTDAnalysisAlgBase::getTruthVertex(const xAOD::TrackParticle* track,
                                    bool& is_hs) {
  is_hs = false;
  auto truth_particle = getTruthParticle(track);
  if (not truth_particle) {
    return nullptr;
  }

  // check if truth particle is from HS event
  auto truth_hs_event = m_truth_event_container->at(0);
  int n_hs_truthparticles = truth_hs_event->nTruthParticles();
  auto truth_hs_vtx = truth_hs_event->signalProcessVertex();

  // std::vector <unsigned int> index_hs_vec;
  // index_hs_vec.reserve(n_hs_truthparticles);
  for (int i = 0; i < n_hs_truthparticles; i++) {
    if (truth_hs_event->truthParticle(i)->barcode() < 200000 and
        truth_hs_event->truthParticle(i)->barcode() != 0 and
        truth_hs_event->truthParticle(i)->status() == 1 and
        truth_hs_event->truthParticle(i)->isCharged()) {
      // index_hs_vec.push_back(truth_hs_event->truthParticle(i)->index());
      if (truth_hs_event->truthParticle(i)->index() ==
          truth_particle->index()) {
        is_hs = true;
        return truth_hs_vtx;
      }
    }
  }
  // for (size_t i = 0; i < index_hs_vec.size(); i++) {
  //   if(truth_particle->index() == index_hs_vec.at(i)) {
  //     is_hs = true;
  //     return truth_hs_vtx;
  //   }
  // }

  // if not, then check if from PU event
  for (size_t pu_event = 0; pu_event < m_pileup_truth_container->size();
       pu_event++) {
    auto truth_pu_event = m_pileup_truth_container->at(pu_event);
    auto truth_pu_vertex = truth_pu_event->truthVertex(1);
    int n_pu_truthparticles = truth_pu_event->nTruthParticles();

    // std::vector <unsigned int> index_pu_vec;
    // index_pu_vec.reserve(n_pu_truthparticles);
    for (int i = 0; i < n_pu_truthparticles; i++) {
      if (truth_pu_event->truthParticle(i)->barcode() < 200000 and
          truth_pu_event->truthParticle(i)->barcode() != 0 and
          truth_pu_event->truthParticle(i)->status() == 1 and
          truth_pu_event->truthParticle(i)->isCharged()) {
        // index_pu_vec.push_back(truth_pu_event->truthParticle(i)->index());
        if (truth_particle->index() ==
            truth_pu_event->truthParticle(i)->index()) {
          return truth_pu_vertex;
        }
      }
    }
    // for (size_t i = 0; i < index_pu_vec.size(); i++) {
    // if(truth_particle->index() == index_pu_vec.at(i)) {
    // return truth_pu_vertex;
    // }
    // }
  } // LOOP PU events
  // can't be associated to any vertex
  return nullptr;
}

double
HGTDAnalysisAlgBase::vertexTruthTime(const xAOD::TruthVertex* truth_vertex) {
  if (truth_vertex) {
    return truth_vertex->t() / Gaudi::Units::c_light; // speed of light in mm/ns
  }
  // return a default value if it can't be associated to any vertex
  return -999.;
}

double HGTDAnalysisAlgBase::trackTruthTime(const xAOD::TrackParticle* track,
                                           bool& is_hs) {

  auto truth_vertex = getTruthVertex(track, is_hs);

  if (truth_vertex) {
    return truth_vertex->t() / Gaudi::Units::c_light;
  }
  // return a default value if it can't be associated to any vertex
  return -999.;
}

double HGTDAnalysisAlgBase::parametrisedLocalVertexDensity(
    const xAOD::Vertex* vertex) {
  double vx_z = vertex->z();
  double density =
      (4. / (sqrt(Gaudi::Units::twopi))) * exp(-0.5 * vx_z * vx_z / 2500.);
  return density;
}

const xAOD::Vertex* HGTDAnalysisAlgBase::getPrimaryRecoVertex() {
  const xAOD::Vertex* primary_vertex = nullptr;
  if (m_primary_vertex_container == nullptr) {
    return primary_vertex;
  }
  for (const auto& vx : *m_primary_vertex_container) {
    if (vx->vertexType() == xAOD::VxType::VertexType::PriVtx) {
      primary_vertex = vx;
      break;
    }
  }
  return primary_vertex;
}

double HGTDAnalysisAlgBase::localVertexDensity(const xAOD::Vertex* vertex,
                                               double window) {
  int n_close_vertices = 0;
  for (const auto& vx : *m_primary_vertex_container) {
    if (vx == vertex) {
      continue;
    }
    if (fabs(vx->z() - vertex->z()) < window) {
      n_close_vertices++;
    }
  }
  return (double)n_close_vertices / (2. * window);
}

double HGTDAnalysisAlgBase::localTruthVertexDensity(const xAOD::Vertex* vertex,
                                                    double window) {
  int n_close_vertices = 0;

  for (size_t pu_event = 0; pu_event < m_pileup_truth_container->size();
       pu_event++) {
    auto truth_pu_event = m_pileup_truth_container->at(pu_event);
    auto truth_pu_vertex = truth_pu_event->truthVertex(1);
    if (fabs(truth_pu_vertex->z() - vertex->z()) < window) {
      n_close_vertices++;
    }
  } // LOOP PU events

  return (double)n_close_vertices / (2. * window);
}

int HGTDAnalysisAlgBase::nRecoVertices() {
  if (m_primary_vertex_container == nullptr) {
    return 0;
  }
  return m_primary_vertex_container->size();
}

const xAOD::TruthVertex* HGTDAnalysisAlgBase::getTruthHSVertex() {
  const xAOD::TruthVertex* truth_hs_vertex = nullptr;
  if (m_truth_event_container == nullptr) {
    return truth_hs_vertex;
  }
  if (m_truth_event_container->size() < 1) {
    ATH_MSG_WARNING(
        "[HGTDAnalysisAlgBase::getTruthHSVertex] no HS vertex found!");
    return truth_hs_vertex;
  }
  // truth event has only on entry
  truth_hs_vertex = m_truth_event_container->at(0)->signalProcessVertex();
  return truth_hs_vertex;
}

std::vector<const xAOD::TruthParticle*>
HGTDAnalysisAlgBase::getTruthParticles(const xAOD::TruthEvent* truth_event,
                                       float min_pt_cut) {
  std::vector<const xAOD::TruthParticle*> truth_particles;

  int n_truthparticles = truth_event->nTruthParticles();
  truth_particles.reserve(n_truthparticles);

  for (int i = 0; i < n_truthparticles; i++) {
    if (truth_event->truthParticle(i)->barcode() < 200000 and
        truth_event->truthParticle(i)->barcode() != 0 and
        truth_event->truthParticle(i)->status() == 1 and
        truth_event->truthParticle(i)->isCharged() and
        truth_event->truthParticle(i)->pt() >= min_pt_cut and
        truth_event->truthParticle(i)->eta() < 4.) {
      truth_particles.push_back(truth_event->truthParticle(i));
    }
  }
  return truth_particles;
}

std::vector<const xAOD::TruthParticle*> HGTDAnalysisAlgBase::getTruthParticles(
    const xAOD::TruthPileupEvent* truth_event, float min_pt_cut) {
  std::vector<const xAOD::TruthParticle*> truth_particles;

  int n_truthparticles = truth_event->nTruthParticles();
  truth_particles.reserve(n_truthparticles);

  for (int i = 0; i < n_truthparticles; i++) {
    if (truth_event->truthParticle(i)->barcode() < 200000 and
        truth_event->truthParticle(i)->barcode() != 0 and
        truth_event->truthParticle(i)->status() == 1 and
        truth_event->truthParticle(i)->isCharged() and
        truth_event->truthParticle(i)->pt() >= min_pt_cut and
        truth_event->truthParticle(i)->eta() < 4.) {
      truth_particles.push_back(truth_event->truthParticle(i));
    }
  }
  return truth_particles;
}

bool HGTDAnalysisAlgBase::passTrackVertexAssociation(
    const xAOD::TrackParticle* track, const xAOD::Vertex* vertex) {
  return passTrackVertexAssociation(track, vertex, m_track_pt_cut / 1.e3);
}

bool HGTDAnalysisAlgBase::passTrackVertexAssociation(
    const xAOD::TrackParticle* track, const xAOD::Vertex* vertex,
    double min_trk_pt /*GeV*/) {
  double pt = track->pt() / 1.e3;
  double eta = fabs(track->eta());
  if (pt < min_trk_pt) {
    return false;
  }
  if (eta > 4.) {
    return false;
  }
  std::vector<double> p_v;

  if (pt <= 1.5) {
    p_v = {0.0622666, -0.0727512, 0.323766, -0.479161,
           0.347524,  -0.103268,  0.0111874};
  }
  if ((pt > 1.5) && (pt <= 2.5)) {
    p_v = {0.039075, -0.0449015, 0.198707,  -0.289311,
           0.206221, -0.0607471, 0.00655867};
  }
  if ((pt > 2.5) && (pt <= 5.0)) {
    p_v = {0.0267551, -0.0303344, 0.132752,  -0.188808,
           0.131231,  -0.0381548, 0.00409764};
  }
  if ((pt > 5) && (pt <= 10)) {
    p_v = {0.0182236, -0.0206502, 0.0879188, -0.119813,
           0.0794155, -0.0224977, 0.00238907};
  }
  if ((pt > 10)) {
    p_v = {0.0136149, -0.0159818, 0.0648501, -0.0833277,
           0.051544,  -0.0140104, 0.00145876};
  }

  if (eta < 2.5) {
    eta = 2.5;
  }

  double z0_res_param = p_v.at(0) + p_v.at(1) * eta + p_v.at(2) * pow(eta, 2) +
                        p_v.at(3) * pow(eta, 3) + p_v.at(4) * pow(eta, 4);
  z0_res_param += p_v.at(5) * pow(eta, 5) + p_v.at(6) * pow(eta, 6);
  z0_res_param *= 2.5;

  z0_res_param = fabs(z0_res_param);

  if (fabs(track->z0() + track->vz() - vertex->z()) >= z0_res_param) {
    return false;
  }
  if (fabs(track->z0() - track->vz()) > 250)
    return false;
  if (fabs(track->eta()) < 2.7) {
    if (fabs(track->d0()) > 2) {
      return false;
    }
  } else if (fabs(track->d0()) > 10) {
    return false;
  }
  return true;
}

bool HGTDAnalysisAlgBase::passTrackVertexAssociation(
    const xAOD::TrackParticle* track, const xAOD::TruthVertex* vertex) {
  double pt = track->pt() / 1.e3;
  double eta = fabs(track->eta());
  if (eta > 4.) {
    return false;
  }
  if (pt < m_track_pt_cut / 1.e3) {
    return false;
  }
  std::vector<double> p_v;

  if (pt <= 1.5) {
    p_v = {0.0622666, -0.0727512, 0.323766, -0.479161,
           0.347524,  -0.103268,  0.0111874};
  }
  if ((pt > 1.5) && (pt <= 2.5)) {
    p_v = {0.039075, -0.0449015, 0.198707,  -0.289311,
           0.206221, -0.0607471, 0.00655867};
  }
  if ((pt > 2.5) && (pt <= 5.0)) {
    p_v = {0.0267551, -0.0303344, 0.132752,  -0.188808,
           0.131231,  -0.0381548, 0.00409764};
  }
  if ((pt > 5) && (pt <= 10)) {
    p_v = {0.0182236, -0.0206502, 0.0879188, -0.119813,
           0.0794155, -0.0224977, 0.00238907};
  }
  if ((pt > 10)) {
    p_v = {0.0136149, -0.0159818, 0.0648501, -0.0833277,
           0.051544,  -0.0140104, 0.00145876};
  }

  if (eta < 2.5) {
    eta = 2.5;
  }

  double z0_res_param = p_v.at(0) + p_v.at(1) * eta + p_v.at(2) * pow(eta, 2) +
                        p_v.at(3) * pow(eta, 3) + p_v.at(4) * pow(eta, 4);
  z0_res_param += p_v.at(5) * pow(eta, 5) + p_v.at(6) * pow(eta, 6);
  z0_res_param *= 2.5;

  z0_res_param = fabs(z0_res_param);

  if (fabs(track->z0() + track->vz() - vertex->z()) >= z0_res_param) {
    return false;
  }
  if (fabs(track->z0() - track->vz()) > 250)
    return false;
  if (fabs(track->eta()) < 2.7) {
    if (fabs(track->d0()) > 2) {
      return false;
    }
  } else if (fabs(track->d0()) > 10) {
    return false;
  }
  return true;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::vertexAssociatedTracks(
    const xAOD::Vertex* vertex, bool (*passes)(const xAOD::TrackParticle* track,
                                               const xAOD::Vertex* vertex)) {
  std::vector<const xAOD::TrackParticle*> good_tracks;
  good_tracks.reserve((*m_track_container).size());
  for (const auto& trk : *m_track_container) {
    if ((*passes)(trk, vertex)) {
      good_tracks.push_back(trk);
    }
  }
  return good_tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::vertexAssociatedTracks(
    std::vector<const xAOD::TrackParticle*> tracks, const xAOD::Vertex* vertex,
    bool (*passes)(const xAOD::TrackParticle* track,
                   const xAOD::Vertex* vertex)) {
  std::vector<const xAOD::TrackParticle*> good_tracks;
  good_tracks.reserve(tracks.size());
  for (const auto& trk : tracks) {
    if (passes(trk, vertex)) {
      good_tracks.push_back(trk);
    }
  }
  return good_tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::vertexAssociatedHGTDTracks(
    std::vector<const xAOD::TrackParticle*> tracks, const xAOD::Vertex* vertex,
    bool (*passes)(const xAOD::TrackParticle* track,
                   const xAOD::Vertex* vertex)) {
  std::vector<const xAOD::TrackParticle*> good_tracks;
  good_tracks.reserve(tracks.size());
  for (const auto& trk : tracks) {
    if (fabs(trk->eta()) < 2.4) {
      continue;
    }
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passes(trk, vertex)) {
      good_tracks.push_back(trk);
    }
  }
  return good_tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::vertexAssociatedHGTDTracks(
    const xAOD::Vertex* vertex, bool (*passes)(const xAOD::TrackParticle* track,
                                               const xAOD::Vertex* vertex)) {
  std::vector<const xAOD::TrackParticle*> good_tracks;
  good_tracks.reserve(1000);
  for (const auto& trk : *m_track_container) {
    if (fabs(trk->eta()) < 2.4) {
      continue;
    }
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passes(trk, vertex)) {
      good_tracks.push_back(trk);
    }
  }
  return good_tracks;
}

std::vector<const xAOD::TrackParticle*>
HGTDAnalysisAlgBase::vertexAssociatedHGTDTracks(const xAOD::Vertex* vertex,
                                                double min_trk_pt) {
  std::vector<const xAOD::TrackParticle*> good_tracks;
  good_tracks.reserve(1000);
  for (const auto& trk : *m_track_container) {
    if (fabs(trk->eta()) < 2.4) {
      continue;
    }
    if (trk->pt() < m_track_pt_cut) {
      continue;
    }
    if (passTrackVertexAssociation(trk, vertex, min_trk_pt)) {
      good_tracks.push_back(trk);
    }
  }
  return good_tracks;
}

bool HGTDAnalysisAlgBase::extrapolateTrackFromLastMeasurement(
    const xAOD::TrackParticle* track, TVector3& v, TVector3& vp, int surface) {

  // Get last measurement of the track
  unsigned int index = 0;
  Trk::CurvilinearParameters temp;
  if (track->indexOfParameterAtPosition(index, xAOD::LastMeasurement)) {
    temp = track->curvilinearParameters(index);
  } else {
    ATH_MSG_WARNING("[HGTDAnalysisAlgBase::extrapolateTrackFromLastMeasurement]"
                    " Track  does not contain last measurement on track");
    return false;
  }

  const Trk::TrackParameters* tp_last_measurement = &temp;
  const Trk::TrackParameters* tp_chosen_surface = nullptr;
  Trk::ParticleHypothesis particle_hyp = Trk::pion;

  if (track->eta() > 0) {
    tp_chosen_surface = m_extrapolator->extrapolate(
        *tp_last_measurement, *(m_disc_positive_z.at(surface)),
        Trk::alongMomentum, true, particle_hyp);
  } else {
    tp_chosen_surface = m_extrapolator->extrapolate(
        *tp_last_measurement, *(m_disc_negative_z.at(surface)),
        Trk::alongMomentum, true, particle_hyp);
  }

  if (tp_chosen_surface != nullptr) {
    double x = tp_chosen_surface->position().x();
    double y = tp_chosen_surface->position().y();
    double z = tp_chosen_surface->position().z();

    double px = tp_chosen_surface->momentum().x();
    double py = tp_chosen_surface->momentum().y();
    double pz = tp_chosen_surface->momentum().z();

    v.SetXYZ(x, y, z);
    vp.SetXYZ(px, py, pz);
    delete tp_chosen_surface;
    return true;
  }

  return false;
}

void HGTDAnalysisAlgBase::setupSurfacesForExtrapolation() {

  for (int i = 0; i < 5; i++) {
    auto translate_posz =
        new Amg::Transform3D(Amg::Vector3D(0., 0., m_hgtd_layer_z.at(i)));
    auto translate_negz =
        new Amg::Transform3D(Amg::Vector3D(0., 0., -m_hgtd_layer_z.at(i)));

    m_disc_positive_z.at(i) = new Trk::DiscSurface(translate_posz, 0., 10000.);
    m_disc_negative_z.at(i) = new Trk::DiscSurface(translate_negz, 0., 10000.);
  }
}
#endif
