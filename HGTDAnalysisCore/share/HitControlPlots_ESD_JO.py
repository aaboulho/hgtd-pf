############################
#
# This Algorithm needs to run on ESDs!
#
############################
from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

ServiceMgr.AuditorSvc.Auditors += ["ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

from PartPropSvc.PartPropSvcConf import PartPropSvc

include("ParticleBuilderOptions/McAOD_PoolCnv_jobOptions.py")
include("EventAthenaPool/EventAthenaPool_joboptions.py")

# build GeoModel for step3.1 with HGTD tweak
DetDescrVersion = 'ATLAS-P2-ITK-17-04-02'
from AthenaCommon.DetFlags import DetFlags
DetFlags.ID_setOn()
DetFlags.TRT_setOff()
DetFlags.Calo_setOn()
DetFlags.HGTD_setOn()
DetFlags.Muon_setOn()
DetFlags.Truth_setOn()

from AthenaCommon.GlobalFlags import globalflags
globalflags.DetDescrVersion = DetDescrVersion

from TrkDetDescrSvc.TrkDetDescrJobProperties import TrkDetFlags
TrkDetFlags.InDetBuildingOutputLevel = VERBOSE

include("InDetSLHC_Example/preInclude.SLHC.py")
SLHC_Flags.LayoutOption = 'InclinedAlternative'
SLHC_Flags.doGMX.set_Value_and_Lock(True)

from AtlasGeoModel import SetGeometryVersion
from AtlasGeoModel import GeoModelInit

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()
ServiceMgr += GeoModelSvc
GeoModelSvc.AtlasVersion = DetDescrVersion

include("InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py")
include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')

#----------------------------
# Input Dataset
#----------------------------
import os
from glob import glob
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags


###Step 3.0 sample
# athenaCommonFlags.FilesInput = glob( "/eos/atlas/atlascerngroupdisk/det-hgtd/samples/2019-03-04-ESD-VBFHinv_mu200/ESD.pool.root.*" )
# athenaCommonFlags.FilesInput = glob("/eos/atlas/atlastier0/tzero/scratch/physmon/dev_noemi/ESDS_AnalogClustering_mu200_NEWDIGI_Feb28/VBFH125_ZZ4nu/ESD.Step3ITk.anal.HGTD.VBFH125_ZZ4nu.*.mu200.pool.root")
# rootFileName = "HitControlPlots_Step3p0.root"

###Step 3.1 sample
athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.ESD.e4956_s3490_s3491_r11670_tid19622728_00/*root.1" )
# athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/*root.1" )
#
############################
#### single muon, mu=0, pt=45, low eta
# athenaCommonFlags.FilesInput = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.415021.ParticleGun_single_muon_Pt45_etaFlatnp23_32.recon.ESD.e5580_s3490_s3491_r11671_tid19622764_00/*root.1")
############################
#### single muon, mu=0, pt=45, high eta
# athenaCommonFlags.FilesInput = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.415022.ParticleGun_single_muon_Pt45_etaFlatnp32_43.recon.ESD.e5580_s3490_s3491_r11671_tid19622771_00/*root.1")
############################
#### single pion, pt=20, mu=0
# athenaCommonFlags.FilesInput = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.415019.ParticleGun_single_piplus_Pt20_etaFlatp23_43.recon.ESD.e5580_s3490_s3491_r11671_tid19622740_00/*root.1")
############################
#### single pion, pt=0.1-5, mu=0
# athenaCommonFlags.FilesInput = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.415081.ParticleGun_single_piplus_Pt0p1_5p0_etaFlatp23_41.recon.AOD.e7135_s3490_s3491_r11671_tid19622753_00/*root.1")
#### minbias high
# athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.recon.ESD.e1133_s3490_s3491_r11671_tid20357562_00/*root.1" )


rootFileName = "HitControlPlots.root"
# rootFileName = "HitControlPlots_Step3p1_singlemuon_ESD.root"

# theApp.EvtMax = 10
theApp.EvtMax = 100


ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()


#----------------------------
# Message Service
#----------------------------
# set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 9999999
ServiceMgr.MessageSvc.setError +=  [ "HepMcParticleLink"]

#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import HitControlPlots
topSequence += HitControlPlots("HitControlPlots", OutputLevel=ERROR)

#---------------------------
# Detector geometry
#---------------------------
from RecExConfig.AutoConfiguration import *
ConfigureFieldAndGeo()
include("RecExCond/AllDet_detDescr.py")

include("InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py")


# analysis and output histos

svcMgr += CfgMgr.THistSvc()
histSvcString = "Hits DATAFILE='{0}' OPT='RECREATE'".format(rootFileName)
svcMgr.THistSvc.Output += [histSvcString]
