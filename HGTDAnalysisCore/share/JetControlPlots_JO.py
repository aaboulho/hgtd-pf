import AthenaPoolCnvSvc.ReadAthenaPool



from glob import glob

# svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/*")

svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3490_s3491_r11672_tid19655941_00/*")

# svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e5458_s3490_s3491_r11670_tid19622734_00/*pool.root.1")

theApp.EvtMax = 100

# algSeq = CfgMgr.AthSequencer("AthAlgSeq")
# algSeq += CfgMgr.JetControlPlots("JetControlPlots", OutputLevel=DEBUG)

#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import JetControlPlots
jetControlPlots = JetControlPlots()

jetControlPlots.GlobalWeight = 1.

topSequence += jetControlPlots
#----------------------------
# Tools
#----------------------------

# Tool for jet calibration
ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=False
ToolSvc.myJESTool.ConfigFile="JES_HLLHC_mu200_r9589_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_Origin_EtaJES"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo"


from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

output_command_string = "Jets DATAFILE='JetControlPlots_Output.root' OPT='RECREATE'"
ServiceMgr.THistSvc.Output += [output_command_string]
