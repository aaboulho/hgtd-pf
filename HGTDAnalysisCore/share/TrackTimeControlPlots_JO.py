import AthenaPoolCnvSvc.ReadAthenaPool

from glob import glob
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

### step 3.0
# svcMgr.EventSelector.InputCollections = glob( "/eos/atlas/atlascerngroupdisk/det-hgtd/samples/2019-03-04-ESD-VBFHinv_mu200/ESD.pool.root.*" )
# step = "3p0"
# sensor_resolution = 0.03

### step3.1
######
# svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/AOD.19622728.*.pool.root.1")
##
athenaCommonFlags.FilesInput  = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/AOD.19622728.*.pool.root.1")

# athenaCommonFlags.FilesInput  = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.ESD.e4956_s3490_s3491_r11670_tid19622728_00/ESD.19622728.*.pool.root.1")


ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()
######
step = "3p1"
sensor_resolution = 0.035

theApp.EvtMax = 100



#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import TrackTimeControlPlots
trackControlPlots = TrackTimeControlPlots("TrackTimeControlPlots", OutputLevel=DEBUG)

trackControlPlots.SensorResolution = sensor_resolution
trackControlPlots.GeometryStep = step

topSequence += trackControlPlots
#----------------------------
# Tools
#----------------------------


from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

output_command_string = "TrackTimes DATAFILE='TrackTimesControlPlots_{0}_Output.root' OPT='RECREATE'".format(step)
ServiceMgr.THistSvc.Output += [output_command_string]
