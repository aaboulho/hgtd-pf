import AthenaPoolCnvSvc.ReadAthenaPool



from glob import glob

svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/*")

theApp.EvtMax = 2000
#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import VertexControlPlots
vertexControlPlots = VertexControlPlots("VertexControlPlots", OutputLevel=ERROR)
vertexControlPlots.clusterDistance = 4.0

topSequence += vertexControlPlots


from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

output_command_string = "VertexControlPlots DATAFILE='VertexControlPlots.root' OPT='RECREATE'"
ServiceMgr.THistSvc.Output += [output_command_string]
