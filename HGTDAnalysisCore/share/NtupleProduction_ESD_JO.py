############################
#
# This Algorithm needs to run on ESDs!
#
############################
from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

ServiceMgr.AuditorSvc.Auditors += ["ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

from PartPropSvc.PartPropSvcConf import PartPropSvc

include("ParticleBuilderOptions/McAOD_PoolCnv_jobOptions.py")
include("EventAthenaPool/EventAthenaPool_joboptions.py")

# build GeoModel for step3.1 with HGTD tweak
DetDescrVersion = 'ATLAS-P2-ITK-17-04-02'
from AthenaCommon.DetFlags import DetFlags
DetFlags.ID_setOn()
DetFlags.TRT_setOff()
DetFlags.Calo_setOn()
DetFlags.HGTD_setOn()
DetFlags.Muon_setOn()
DetFlags.Truth_setOn()

from AthenaCommon.GlobalFlags import globalflags
globalflags.DetDescrVersion = DetDescrVersion

from TrkDetDescrSvc.TrkDetDescrJobProperties import TrkDetFlags
TrkDetFlags.InDetBuildingOutputLevel = VERBOSE

include("InDetSLHC_Example/preInclude.SLHC.py")
SLHC_Flags.LayoutOption = 'InclinedAlternative'
SLHC_Flags.doGMX.set_Value_and_Lock(True)

from AtlasGeoModel import SetGeometryVersion
from AtlasGeoModel import GeoModelInit

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()
ServiceMgr += GeoModelSvc
GeoModelSvc.AtlasVersion = DetDescrVersion

include("InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py")
include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')

#----------------------------
# Input Dataset
#----------------------------
import os
from glob import glob
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags



# athenaCommonFlags.FilesInput = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.ESD.e4956_s3490_s3491_r11670_tid19622728_00/*")

# athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.recon.ESD.e1133_s3490_s3491_r11671_tid20357562_00/*root.1" )

athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.415022.ParticleGun_single_muon_Pt45_etaFlatnp32_43.recon.ESD.e5580_s3490_s3491_r11670_tid19622615_00/*root.1" )

theApp.EvtMax = 5

ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()

#----------------------------
# Message Service
#----------------------------
# set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 9999999
ServiceMgr.MessageSvc.setError +=  [ "HepMcParticleLink"]

#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import NtupleProduction
ntupleProduction = NtupleProduction("NtupleProduction", OutputLevel=WARNING)

ntupleProduction.JetMinPtCut = 20.0
ntupleProduction.TrackMinPtCut = 1.0
ntupleProduction.IncludeHits = True

topSequence += ntupleProduction

#---------------------------
# Detector geometry
#---------------------------
from RecExConfig.AutoConfiguration import *
ConfigureFieldAndGeo()
include("RecExCond/AllDet_detDescr.py")

include("InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py")

#----------------------------
# TOOLS
#----------------------------


# Tool for jet calibration
ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=False
ToolSvc.myJESTool.ConfigFile="JES_HLLHC_mu200_r9589_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_Origin_EtaJES"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo"

# ToolSvc += CfgMgr.TruthSelectionTool("myTSTool")
# ToolSvc.myTSTool.maxEta = 4.0
# ToolSvc.myTSTool.minPt = 1000

#---------------------------
# Output histograms and trees
#---------------------------

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

output_command_string = "NTUPLE DATAFILE='HGTD_ntuple.root' OPT='RECREATE'"
ServiceMgr.THistSvc.Output += [output_command_string]
