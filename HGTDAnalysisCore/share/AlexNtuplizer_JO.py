#from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

from PartPropSvc.PartPropSvcConf import PartPropSvc

include( "ParticleBuilderOptions/McAOD_PoolCnv_jobOptions.py")
include( "EventAthenaPool/EventAthenaPool_joboptions.py" )

import os
from glob import glob
from AthenaCommon.AthenaCommonFlags  import athenaCommonFlags
# athenaCommonFlags.FilesInput = glob( "/eos/home-a/albasan/muons_45GeV_eta_24_40/hgtd_hits-5.pool.root" ) # HITS
# athenaCommonFlags.FilesInput = glob( "/afs/cern.ch/user/d/dzerwas/work/public/20.20.14.1/hgtd.AOD.pool.root" ) # AOD
# athenaCommonFlags.FilesInput = glob("/eos/user/a/aleopold/hgtd/tdr_tests/step3p0/hgtd.AOD_*_dijet_step3p0.pool.root") # AOD

#single muon, step3 tests
# athenaCommonFlags.FilesInput = glob("/afs/cern.ch/user/a/aleopold/hgtd_0.AOD.pool.root")

# single muon
# athenaCommonFlags.FilesInput = glob( "/eos/atlas/atlascerngroupdisk/det-hgtd/samples/aod/20.20.14.1/Muons/MU0_FlatR/mc15_14TeV.415023.ParticleGun_single_muon_Pt45_thetaFlat.recon.AOD.e6483_s3388_s3389_r11134/AOD.16486125._*.pool.root.1" ) # AOD

# athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/tdr_tests/mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3388_s3389_r11134_tid16538513_00/AOD.16538513._000245.pool.root.1" ) # AOD

# athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/tdr_tests/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3388_s3389_r11252_tid17185529_00/AOD.17185529._003369.pool.root.1" ) # AOD

#piplus mu0
# athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/tdr_tests/mc15_14TeV.415081.ParticleGun_single_piplus_Pt0p1_5p0_etaFlatp23_41.recon.AOD.e7135_s3388_s3389_r11134_tid16486070_00/AOD.16486070._000032.pool.root.1" )
#piplus mu200
#athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/tdr_tests/mc15_14TeV.415019.ParticleGun_single_piplus_Pt20_etaFlatp23_43.recon.AOD.e5580_s3388_s3389_r11135_tid16663668_00/AOD.16663668._004110.pool.root.1" )

athenaCommonFlags.FilesInput = glob( "/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.ESD.e4956_s3490_s3491_r11670_tid19622728_00/ESD.19622728.*.pool.root.1" ) # AOD


ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput() # This is stupid and redundant, but necessary

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import HGTDHitAnalysis
HGTDHitAnalysis = HGTDHitAnalysis()
topSequence += HGTDHitAnalysis

HGTDHitAnalysis.NtupleFileName = ''

HGTDHitAnalysis.IsSignleParticle = True
HGTDHitAnalysis.DebugMode = False

# HGTDHitAnalysis.HGTDHitsContainerType = 'LArHitHGTD' # 'HGTDDigitContainer_MC' for RDO input (default), 'LArHitHGTD' for HITS input
HGTDHitAnalysis.HGTDHitsContainerType = 'HGTDDigitContainer_MC' # 'HGTDDigitContainer_MC' for RDO input (default), 'LArHitHGTD' for HITS input
HGTDHitAnalysis.HGTDHitsGranularity = 1.3 # granularity of the HGTD sensors, 1.0 or 1.3 mm
HGTDHitAnalysis.TruthParticleType = 13 # pdgId of the truth single particle, 0 : None

HGTDHitAnalysis.HGTDUsePulseSimulation = True
HGTDHitAnalysis.HGTDTimingScenario = "Initial"
# HGTDHitAnalysis.HGTDTimingScenario = "Intermediate"
# HGTDHitAnalysis.HGTDTimingScenario = "Final"
# HGTDHitAnalysis.HGTDTimingScenario = "Luminosity"
HGTDHitAnalysis.HGTDLuminosity = 1000 # If HGTDTimingScenario = "Luminosity", select the luminosity used to compute the timing resolution


HGTDHitAnalysis.UseHits = False

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

# ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis.root' OPT='RECREATE'" ]

ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_Initial.root' OPT='RECREATE'" ]
# ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_1000fb.root' OPT='RECREATE'" ]
# ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_Intermediate.root' OPT='RECREATE'" ]
# ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_2001fb.root' OPT='RECREATE'" ]
# ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_3000fb.root' OPT='RECREATE'" ]
# ServiceMgr.THistSvc.Output += ["HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_Final.root' OPT='RECREATE'" ]

ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 9999999

# theApp.EvtMax = -1
theApp.EvtMax = 2

ServiceMgr.AuditorSvc.Auditors  += [ "ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

# To set up a geometry
#include("RecExCond/RecExCommon_DetFlags.py")
from AthenaCommon.DetFlags  import DetFlags
DetFlags.dcs.all_setOn()
DetFlags.detdescr.all_setOn()
DetFlags.makeRIO.all_setOn()
# these have to be set off by hand!
DetFlags.TRT_setOff()
DetFlags.pixel_setOn() # change from off to on
DetFlags.detdescr.BField_setOn() # just added
DetFlags.detdescr.SCT_setOn() # just added
# these have to be set On by hand!
DetFlags.dcs.HGTD_setOn()
DetFlags.detdescr.HGTD_setOn()
DetFlags.makeRIO.HGTD_setOn()

DetDescrVersion = 'ATLAS-P2-ITK-17-04-02'
from AthenaCommon.GlobalFlags import globalflags
globalflags.DetDescrVersion = DetDescrVersion

# ITK step 3
from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags
SLHC_Flags.doGMX.set_Value_and_Lock(True)
SLHC_Flags.LayoutOption="InclinedAlternative"
# endof ITK step 3

# Tool for jet calibration
ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=False
ToolSvc.myJESTool.ConfigFile="JES_HLLHC_mu200_r9589_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_Origin_EtaJES"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo"


# To set up a geometry
from RecExConfig.AutoConfiguration import *
ConfigureFieldAndGeo() # Configure the settings for the geometry
include("RecExCond/AllDet_detDescr.py") # Actually load the geometry
#include( "TrkDetDescrSvc/AtlasTrackingGeometrySvc.py" ) # Tracking geometry, handy for ID work

# ITK step 3
include("InDetSLHC_Example/preInclude.SLHC.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py")
include("InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py")
include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')
# endof ITK step 3
