############################
#
# TO USE THE EXTRAPOLATOR, YOU NEED TO ADD THE FOLLOWING THINGS
#
############################
from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

ServiceMgr.AuditorSvc.Auditors += ["ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

from PartPropSvc.PartPropSvcConf import PartPropSvc

include("ParticleBuilderOptions/McAOD_PoolCnv_jobOptions.py")
include("EventAthenaPool/EventAthenaPool_joboptions.py")

# build GeoModel for step3.1 with HGTD tweak
DetDescrVersion = 'ATLAS-P2-ITK-17-04-02'
from AthenaCommon.DetFlags import DetFlags
DetFlags.ID_setOn()
DetFlags.TRT_setOff()
DetFlags.Calo_setOn()
DetFlags.HGTD_setOn()
DetFlags.Muon_setOn()
DetFlags.Truth_setOn()

from AthenaCommon.GlobalFlags import globalflags
globalflags.DetDescrVersion = DetDescrVersion

from TrkDetDescrSvc.TrkDetDescrJobProperties import TrkDetFlags
TrkDetFlags.InDetBuildingOutputLevel = VERBOSE

include("InDetSLHC_Example/preInclude.SLHC.py")
SLHC_Flags.LayoutOption = 'InclinedAlternative'
SLHC_Flags.doGMX.set_Value_and_Lock(True)

from AtlasGeoModel import SetGeometryVersion
from AtlasGeoModel import GeoModelInit

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()
ServiceMgr += GeoModelSvc
GeoModelSvc.AtlasVersion = DetDescrVersion

include("InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py")
include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')

#########
#########

import AthenaPoolCnvSvc.ReadAthenaPool

from glob import glob
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

### step 3.0
# svcMgr.EventSelector.InputCollections = glob( "/eos/atlas/atlascerngroupdisk/det-hgtd/samples/2019-03-04-ESD-VBFHinv_mu200/ESD.pool.root.*" )
# step = "3p0"
# sensor_resolution = 0.03

### step3.1
######
# svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/AOD.19622728.*.pool.root.1")
##
# athenaCommonFlags.FilesInput  = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/AOD.19622728.*.pool.root.1")

athenaCommonFlags.FilesInput  = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.ESD.e4956_s3490_s3491_r11670_tid19622728_00/ESD.19622728.*.pool.root.1")


ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()
######
step = "3p1"
sensor_resolution = 0.035

theApp.EvtMax = 10



#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import TrackTimeControlPlots
trackControlPlots = TrackTimeControlPlots("TrackTimeControlPlots", OutputLevel=DEBUG)

trackControlPlots.SensorResolution = sensor_resolution
trackControlPlots.GeometryStep = step

topSequence += trackControlPlots
#----------------------------
# Tools
#----------------------------


from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

output_command_string = "TrackTimes DATAFILE='TrackTimesControlPlots_{0}_Output.root' OPT='RECREATE'".format(step)
ServiceMgr.THistSvc.Output += [output_command_string]

#---------------------------
# Detector geometry
#---------------------------
from RecExConfig.AutoConfiguration import *
ConfigureFieldAndGeo()
include("RecExCond/AllDet_detDescr.py")

include("InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py")
