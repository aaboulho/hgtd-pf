import AthenaPoolCnvSvc.ReadAthenaPool



from glob import glob

svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3490_s3491_r11670_tid19622728_00/*")

# svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3490_s3491_r11672_tid19655948_00/*")

# Zee
# svcMgr.EventSelector.InputCollections = glob("/eos/atlas/atlascerngroupdisk/det-hgtd/samples/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3490_s3491_r11672_tid19655933_00/*")

# Ztautau
# svcMgr.EventSelector.InputCollections = glob("/eos/user/a/aleopold/hgtd/ESD/mc15_14TeV.147808.PowhegPythia8_AU2CT10_Ztautau.recon.AOD.e2491_s3490_s3491_r11672_tid19655922_00/*")

theApp.EvtMax = 5
#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDAnalysisCore.HGTDAnalysisCoreConf import PileupRejectionNtuple
pileupRejectionAlg = PileupRejectionNtuple("PileupRejectionNtuple", OutputLevel=ERROR)
pileupRejectionAlg.MaxVertexDeltaZ = 2.0
pileupRejectionAlg.LowJetPtCut = 30.0

# pileupRejectionAlg.UsedAccessor = "PFTQv04"
# pileupRejectionAlg.UsedPUjetAccessor = "PFTQv04"
#
# pileupRejectionAlg.UsedAccessor = "PFTQv05"
# pileupRejectionAlg.UsedPUjetAccessor = "PFTQv05"
#
# pileupRejectionAlg.UsedAccessor = "PFTQv05"
# pileupRejectionAlg.UsedPUjetAccessor = "PFTQv04"
#
# pileupRejectionAlg.UsedAccessor = "PFTQv05"
# pileupRejectionAlg.UsedPUjetAccessor = "ProgressiveFilter_1"
#
# pileupRejectionAlg.UsedAccessor = "PFTQv04"
# pileupRejectionAlg.UsedPUjetAccessor = "ProgressiveFilter_1"

# "PFTQv05S_999_0.099_1.5_0.0"
# "PFTQv05S_999_0.100_1.5_0.005"
# "PFTQv05S_999_0.103_1.5_0.01"
# "PFTQv05S_999_0.107_1.5_0.015"
# "PFTQv05S_999_0.114_1.5_0.02"
# "PFTQv05S_999_0.122_1.5_0.025"
# "PFTQv05S_999_0.130_1.5_0.03"
# "PFTQv05S_999_0.140_1.5_0.035"
# "PFTQv05S_999_0.150_1.5_0.04"
# "PFTQv05S_999_0.161_1.5_0.045"
# "PFTQv05S_999_0.172_1.5_0.05"

acc_string = "TTA01_1_1_2.0_1.5_1_3.5_3.9_2500"

pileupRejectionAlg.UsedAccessor = acc_string
pileupRejectionAlg.UsedPUjetAccessor = acc_string
# "PFTQv05S_999_0.172_1.5_0.05"

topSequence += pileupRejectionAlg
#----------------------------
# Tools
#----------------------------

# Tool for jet calibration
ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=False
ToolSvc.myJESTool.ConfigFile="JES_HLLHC_mu200_r9589_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_Origin_EtaJES"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo"


from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

output_command_string = "PileupRejectionNtuple DATAFILE='PileupRejectionNtuple.root' OPT='RECREATE'"
ServiceMgr.THistSvc.Output += [output_command_string]
