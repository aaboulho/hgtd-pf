#ifndef HGTD_HIT_ANALYSIS_H
#define HGTD_HIT_ANALYSIS_H

#include "AthenaBaseComps/AthAlgorithm.h"
// #include "LArSimEvent/LArHitContainer.h"

// #include "HGTDReadoutSim/HGTDHitConversion.h"

#include "TrkExInterfaces/IExtrapolator.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"

#include "InDetIdentifier/PixelID.h"
#include "InDetRIO_OnTrack/PixelClusterOnTrack.h"

#include "TMath.h"
#include "TString.h"
#include "TTree.h"

#include "GaudiKernel/ToolHandle.h"
// #include "HGTDReadoutSim/HGTDTool.h"
// #include "HGTDHitAnalysis/HGTDTrack.h"
// #include "HGTDHitAnalysis/HGTDTruth.h"

// additional includes

#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODTruth/TruthPileupEventContainer.h"
// #include "HGTDTrackTimeInterface/TrackTimePFORv02.h"
// #include "HGTDTrackTimeInterface/TruthTrackTimePrimariesAcc.h"

#include "HGTDTrackTimeInterface/AccessorFactory.h"

#include <algorithm>
#include <functional>
#include <iostream>
#include <math.h>

namespace InDetDD {
class PixelDetectorManager;
}

class TTree;
class ITHistSvc;

class HGTDHitAnalysis : public AthAlgorithm {

public:
  HGTDHitAnalysis(const std::string &name, ISvcLocator *pSvcLocator);
  ~HGTDHitAnalysis();

  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual StatusCode execute();

  /**
   * @brief check if a track comes from the truth HS vertex
   */
  bool
  trackParticleOriginatesFromHSVertex(const xAOD::TrackParticle *track_particle,
                                      Int_t &pdg_id, Int_t &barcode);

  Double_t trackTruthTime(const xAOD::TrackParticle *track_particle);
  Double_t trackTruthVXz(const xAOD::TrackParticle *track_particle);

  std::vector<std::vector<TLorentzVector>> getTruthPuJets();

  bool includeParticle(const xAOD::TruthParticle *part);

private:
  // Geo for each layer
  // HGTDHitConversion* m_hitConvTool[4];

  const PixelID *m_pixel_ID;

  std::string m_geo;

  // Double_t m_zpos[4] = {3443.445, 3453.555, 3468.445, 3478.555};

  const InDet::PixelClusterContainer *m_cluster_container;

  // Access the truth event //
  const xAOD::TruthEventContainer *m_xTruthEventContainer = nullptr;
  // const xAOD::TrackParticleContainer * m_trackCont = NULL;

  // Access the truth event //
  const xAOD::TruthVertex *m_vert = nullptr;
  const xAOD::TruthPileupEventContainer *m_pileup_truth_container = nullptr;

  // const DataHandle<LArHitContainer> lar_hit_container;
  const xAOD::JetContainer *m_jet_container = nullptr;
  const xAOD::TrackParticleContainer *m_track_container = nullptr;

  bool m_debug_execute = true;
  bool m_run_jet_code = true;

  std::string m_ntupleFileName;
  std::string m_ntupleDirName;
  std::string m_ntupleTreeName;
  std::string m_hgtdHitsContName;
  bool m_is_single_particle;
  std::string m_hgtdHitsContType;
  Float_t m_granularity;
  bool m_pulse;
  std::string m_timingScenario;
  double m_luminosity;
  Int_t m_truthParticleType;

  std::shared_ptr<ITrackTimeAccessor> m_time_accessor = nullptr;
  std::shared_ptr<ITrackTimeAccessor> m_truth_time_accessor = nullptr;

  // TTree * m_treeHit; // Tree containing informations on each hit
  // Float_t m_coordx; // coordinate X of the hit
  // Float_t m_coordy; // coordinate Y of the hit
  // double m_coordz; // coordinate Z of the hit
  // int m_nsamp; // sampling of the hit
  // Float_t m_coordr; // coordinate R of the hit
  // Float_t m_energy; // energy deposit of the hit
  // Float_t m_time; // time of the hit
  // Float_t m_treco; // time of the hit after correction of t_vertex and tof
  // Float_t m_zposneg; // +1 for one of the EndCap, -1 for the other
  //
  // TTree * m_treeTrack; // Tree containing informations on each event
  // Float_t m_track_eta; // eta of the track
  // Float_t m_track_R; // eta of the track
  // Float_t m_track_Pt; // Pt of the track
  // Float_t m_track_time; // time of the track
  // Float_t m_track_time_res; // time resolution of the track
  // int m_track_hit; // number of hits associated with the track
  bool extrapolateTrackFromLastMeasurement(const xAOD::TrackParticle *track,
                                           TVector3 &pos, TVector3 &mom,
                                           int surface);

  void setupSurfacesForExtrapolation();

  // distances of layers from the IP in mm, numbers for ATLAS-P2-ITK-17-04-02
  std::array<double, 4> m_hgtd_layer_z{
      {3443.8950, 3453.1050, 3468.8950, 3478.1050}};

  std::array<Trk::DiscSurface *, 4> m_disc_positive_z;
  std::array<Trk::DiscSurface *, 4> m_disc_negative_z;

  ITHistSvc *m_thistSvc;

  ToolHandle<Trk::IExtrapolator> m_extrapolator;

  ToolHandle<IJetCalibrationTool> m_jet_calibration_tool;

  Float_t m_hitThreshold; // threshold over which to care about HGTD hits
  Int_t m_nevent;         // number of the event
  bool m_use_hits;

  // const HGTD_ID * m_hgtdID;

  // HGTDTimingResolution m_timingResolutionTool;
  // HGTDTrackExtrapolator m_hgtdExtrapolator;
  //
  // HGTD_TOOL m_hgtdTool;

  TTree *m_data_tree = nullptr;
  // event
  Int_t m_eventnumber;
  Int_t m_runnumber;
  Int_t m_n_vertices;
  Double_t m_vertex_x;
  Double_t m_vertex_y;
  Double_t m_vertex_z;
  std::vector<size_t> m_vertex_track_hashid;
  Double_t m_truth_vertex_x;
  Double_t m_truth_vertex_y;
  Double_t m_truth_vertex_z;
  Double_t m_truth_vertex_time;
  std::vector<Double_t> m_pu_truth_vertex_x;
  std::vector<Double_t> m_pu_truth_vertex_y;
  std::vector<Double_t> m_pu_truth_vertex_z;
  std::vector<Double_t> m_pu_truth_vertex_time;
  // hits
  std::vector<Float_t> m_hit_x;
  std::vector<Float_t> m_hit_y;
  std::vector<Float_t> m_hit_z;
  std::vector<Int_t> m_hit_layer;
  std::vector<Double_t> m_hit_raw_time;
  std::vector<Int_t> m_hit_has_pulse;
  std::vector<Double_t> m_hit_pulse_time;
  std::vector<Double_t> m_hit_pulse_resolution;
  // single particle
  std::vector<Double_t> m_hs_truthparticle_pt;
  std::vector<Double_t> m_hs_truthparticle_e;
  std::vector<Double_t> m_hs_truthparticle_eta;
  std::vector<Double_t> m_hs_truthparticle_phi;
  std::vector<Double_t> m_hs_truthparticle_pdgid;
  std::vector<Int_t> m_hs_truthparticle_charge;

  // tracks
  std::vector<Double_t> m_track_pt;
  std::vector<Double_t> m_track_e;
  std::vector<Double_t> m_track_eta;
  std::vector<Double_t> m_track_phi;
  std::vector<Double_t> m_track_z0;
  std::vector<Double_t> m_track_vz;
  std::vector<Double_t> m_track_d0;
  std::vector<Double_t> m_track_sigr2;
  std::vector<bool> m_track_primes_hastime;
  std::vector<Double_t> m_track_primes_time;
  std::vector<Int_t> m_track_primes_nhits;
  std::vector<bool> m_track_reco_hastime;
  std::vector<Double_t> m_track_reco_time;
  std::vector<Int_t> m_track_reco_nhits;
  std::vector<size_t> m_track_hashid;
  std::vector<Int_t> m_track_pdgid;
  std::vector<Int_t> m_track_barcode;
  std::vector<Int_t> m_track_is_truthmatched;
  std::vector<Int_t> m_track_is_truthstatus; // 0=PU, 1=HS, 2=neither/fake
  std::vector<Double_t> m_track_vx_time;
  std::vector<Double_t> m_track_vx_z;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pos_l0;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pos_l1;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pos_l2;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pos_l3;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pt_l0;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pt_l1;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pt_l2;
  std::vector<std::vector<Double_t>> m_track_extrapolated_pt_l3;
  std::vector<Int_t> m_track_extrapolated_fillfactor_l0;
  std::vector<Int_t> m_track_extrapolated_fillfactor_l1;
  std::vector<Int_t> m_track_extrapolated_fillfactor_l2;
  std::vector<Int_t> m_track_extrapolated_fillfactor_l3;
  // std::vector<Double_t> m_track_time_hitassoc;
  // jets
  std::vector<Double_t> m_jet_pt;
  std::vector<Double_t> m_jet_e;
  std::vector<Double_t> m_jet_m;
  std::vector<Double_t> m_jet_eta;
  std::vector<Double_t> m_jet_phi;
  std::vector<Int_t> m_jet_is_hs;
  std::vector<Int_t> m_jet_is_pu;
  std::vector<Int_t> m_jet_is_qcd;
  std::vector<Int_t> m_jet_is_stoch;
  std::vector<std::vector<size_t>> m_jet_ghost_track_hashid;
};

#endif // HGTD_HIT_ANALYSIS_H
