// #ifndef HGTDANALYSISCORE_JETCONTROLPLOTS_H
// #define HGTDANALYSISCORE_JETCONTROLPLOTS_H 1
//
// #include "AthenaBaseComps/AthAlgorithm.h"
// #include "GaudiKernel/ToolHandle.h"
// #include "GaudiKernel/ITHistSvc.h"
//
// #include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"
// #include "HGTDTrackTimeInterface/AccessorFactory.h"
// #include "HGTDTrackTimeInterface/ITrackTimeAccessor.h"
//
// #include "JetCalibTools/JetCalibrationTool.h"
// 
// #include <memory>
// #include <vector>
// #include <array>
//
// #include "TEfficiency.h"
// #include "TProfile.h"
// #include "TH1F.h"
// #include "TH2D.h"
// #include "TTree.h"
// #include "TLorentzVector.h"
//
//
// class JetControlPlots: public ::HGTDAnalysisAlgBase {
//  public:
//   JetControlPlots( const std::string& name, ISvcLocator* pSvcLocator );
//   virtual ~JetControlPlots();
//
//   struct AccessorInfo {
//     std::shared_ptr<ITrackTimeAccessor> accessor;
//     std::string name;
//   };
//
//   virtual StatusCode  initialize();
//   virtual StatusCode  execute();
//   virtual StatusCode  finalize();
//
//  private:
//
//   //properties
//   double m_global_weight = 1.;
//   double m_min_jet_pt = 20.;
//   double m_max_vertex_deltaz = 2.0;
//
//   ServiceHandle<ITHistSvc> m_histSvc;
//
//   ToolHandle<IJetCalibrationTool> m_jet_calibration_tool;
//
//   //using the categories "HS", "QCD_PU", "STOCH_PU", "NEITHER", "PU"
//   static const int m_n_categories = 5;
//   std::array<TH1F*, m_n_categories> m_hist_jet_pt;
//   std::array<TH1F*, m_n_categories> m_hist_jet_eta;
//   std::array<TH1F*, m_n_categories> m_hist_jet_phi;
//   std::array<TH1F*, m_n_categories> m_hist_n_ghost_tracks;
//
//   static const int m_n_eta_bins = 8; //0 to 4 in 0.5 steps
//   static const int m_n_pt_bins = 4; //30 to 110 in 20 GeV bins
//   std::array<std::array<TH1F*, m_n_eta_bins>, m_n_pt_bins> m_hist_hs_jet_pt_resolution;
//   TH1F* m_hist_hs_jet_pt_resolution_all = nullptr;
//
//   TH1F* m_pt_binfinder = nullptr;
//   TH1F* m_eta_binfinder = nullptr;
//
//   bool eventPassesVBFSelection(const std::vector<xAOD::Jet*>& pt_ordered_jets,
//     const xAOD::JetContainer* truth_jet_container, double mjj_cut = 0. /*GeV*/);
//
//   TTree* m_tree_jets;
//
//   int m_tree_eventnumber;
//
//   ////////////////////////////////////////
//   // assess the properties of clusters in HS tracks
//   ////////////////////////////////////////
//
//   std::vector<AccessorInfo> m_accessors;
//
//
//   //how many hs/pu tracks do I have in those jets?
//   //how many of them have time information
//   static const int m_n_accessors = 6;
//
//   void ghostTrackProperties(xAOD::Jet* jet, ITrackTimeAccessor* accessor, int accessor_i, HGTDAnalysisAlgBase::JetCategory jet_category, const xAOD::Vertex* vertex);
//
//   std::array<std::array<TH1F*, m_n_accessors>, m_n_categories> m_hist_n_good_timed_hs_ghost_tracks;
//   std::array<std::array<TH1F*, m_n_accessors>, m_n_categories> m_hist_n_good_timed_pu_ghost_tracks;
//
//   void studyClusterProperties(const xAOD::Jet* jet, ITrackTimeAccessor* accessor, int accessor_i, const xAOD::Vertex* vertex);
//
//   std::array<std::array<TH1F*, 2>, m_n_accessors> m_hist_cluster_sumpt;
//   std::array<std::array<TH1F*, 2>, m_n_accessors> m_hist_cluster_sumpt2;
//   std::array<std::array<TH1F*, 2>, m_n_accessors> m_hist_cluster_n_tracks;
//   std::array<std::array<TH1F*, 2>, m_n_accessors> m_hist_cluster_pt_leading;
//   std::array<TH1F*, m_n_accessors> m_hist_n_clusters;
//   std::array<TH1F*, m_n_accessors> m_hist_n_hs_clusters;
//
//   //cluster is a HS cluster, if >50% of the tracks are hs tracks
//   float isHSCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   std::vector<Cluster<const xAOD::TrackParticle*>>
//   clusterGhostTracks(const xAOD::Jet* jet,
//     double cluster_distance, CH::ClusterAlgoType algo,
//     ITrackTimeAccessor* accessor, int accessor_i, const xAOD::Vertex* vertex);
//
// };
//
// #endif //> !HGTDANALYSISCORE_JETCONTROLPLOTS_H
