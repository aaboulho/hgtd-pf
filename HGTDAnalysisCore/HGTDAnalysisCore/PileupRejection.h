// #ifndef HGTDANALYSISCORE_PILEUPREJECTION_H
// #define HGTDANALYSISCORE_PILEUPREJECTION_H 1
//
// #include "AthenaBaseComps/AthAlgorithm.h"
// #include "GaudiKernel/ToolHandle.h"
// #include "GaudiKernel/ITHistSvc.h"
// #include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"
// #include "HGTDAnalysisCore/ClusteringHelper.h"
// #include "HGTDTrackTimeInterface/AccessorFactory.h"
// #include "HGTDTrackTimeInterface/ITrackTimeAccessor.h"
// #include "HGTDTrackTimeInterface/TrackTimePFORv02.h"
// #include "HGTDTrackTimeInterface/TrackTimeProgFiltAcc.h"
// #include "HGTDTrackTimeInterface/TruthTrackTimePrimariesAcc.h"
// #include "HGTDTrackTimeInterface/TruthTrackTimeTunableAcc.h"
//
// #include "JetCalibTools/JetCalibrationTool.h"
// 
// #include <vector>
//
// #include "TEfficiency.h"
// #include "TProfile.h"
// #include "TH1F.h"
// #include "TH2D.h"
// #include "TTree.h"
// #include "TRandom3.h"
//
// class PileupRejection: public ::HGTDAnalysisAlgBase {
//
//   enum EventType {HS, PU, FAKE};
//
//   struct MatchedTrack {
//     const xAOD::TrackParticle* reco_trk;
//     const xAOD::TruthParticle* true_trk;
//     int event_type; //HS PU FAKE
//     float track_weight = 0.0;
//     ElementLink<xAOD::TruthEventBaseContainer> event;
//   };
//
//   struct VertexTimeCluster {
//     std::vector<float> tracktimes;
//     int ntracks = 0;
//     std::vector<MatchedTrack> tracks;
//     float time = 0.0;
//   };
//
//   std::vector<MatchedTrack> collectMatchedTracks(const xAOD::Vertex* vertex);
//
//   bool isHardScatterEvent(const ElementLink<xAOD::TruthEventBaseContainer>& evlink);
//
//   float defaultVxTime(const std::vector<MatchedTrack>& tracks);
//
//   float defaultVxTimeWithAccessor(
//     const std::vector<MatchedTrack>& tracks,
//     ITrackTimeAccessor* accessor);
//
//   float getVertexTime(std::vector<VertexTimeCluster> clusters, std::string sortingType="sumpt2" /*, int ibc*/);
//
//   float getTrackProperties(VertexTimeCluster cluster, std::string prop);
//
//   float calculateClusterTime(VertexTimeCluster cluster);
//
//   // Alex tries as well
//
//   //uses a different clustering
//   float v01VxTime(const xAOD::Vertex* vertex, ITrackTimeAccessor* accessor);
//
//   //selects only hits that have a hit on the surface closest to HGTD
//   float v02VxTime(const xAOD::Vertex* vertex, ITrackTimeAccessor* accessor);
//
//   //track with 1 hit assigned are skipped
//   float v03VxTime(const xAOD::Vertex* vertex, ITrackTimeAccessor* accessor);
//
//   //use a higher pT cut on the tracks and a sumpT2 minimum on clusters of 15GeV2
//   float v04VxTime(const xAOD::Vertex* vertex, ITrackTimeAccessor* accessor, float ptcut);
//
//   //use a higher pT cut on the tracks and no sumpT2 minimum
//   float v05VxTime(const xAOD::Vertex* vertex, ITrackTimeAccessor* accessor, float ptcut);
//
//  public:
//   PileupRejection( const std::string& name, ISvcLocator* pSvcLocator );
//   virtual ~PileupRejection();
//
//   virtual StatusCode  initialize();
//   virtual StatusCode  execute();
//   virtual StatusCode  finalize();
//
//   //produce plots that are needed to parametrise the PU rejection
//   void subJetRpTParametrisation(xAOD::Jet* jet, const xAOD::Vertex* vertex, JetCategory category);
//
//   //find the optimal distance parameter for the sub-jet rpt clustering
//   void subJetRpTOptimisation(xAOD::Jet* jet, const xAOD::Vertex* vertex, JetCategory category);
//
//   void vertexDensityRpTCurves(xAOD::Jet* jet, const xAOD::Vertex* vertex, JetCategory category,  int density_bin);
//
//   void etaRpTCurves(xAOD::Jet* jet, const xAOD::Vertex* vertex, JetCategory category,  int eta_bin);
//
//   void fillRpTEffCurves(float rpt, std::array<TEfficiency*, 4>& eff, JetCategory category, double pt);
//
//
//   //variables for Marianna
//   // takes the leading track of the jet and all tracks in HGTD associated to
//   // the vertex
//   double t0UE(const xAOD::TrackParticle *leading_trk,
//               const std::vector<const xAOD::TrackParticle*> & tracks,
//               ITrackTimeAccessor* accessor);
//
//  private:
//
//   std::string m_directory_name = "";
//
//   int m_n_rpt_bins = 100;
//   double m_rpt_stepsize = 0.01;
//   double m_rpt_offset = 0.001;
//
//   StatusCode setupRocCurvePlots(std::array<TEfficiency*, 4>& arr, const std::string & name, const std::string & x_label);
//
//   double m_max_vertex_deltaz = 2.0;
//   double m_jet_pt_low_cut = 30.;
//   double m_jet_pt_high_cut = 50.;
//   double m_jet_pt_max_cut = 80.;
//
//   ServiceHandle<ITHistSvc> m_histSvc;
//   ToolHandle<IJetCalibrationTool> m_jet_calibration_tool;
//
//   std::shared_ptr<ITrackTimeAccessor> m_time_accessor = nullptr; //most up to date
//   std::shared_ptr<ITrackTimeAccessor> m_time_accessor_pf = nullptr;
//   std::shared_ptr<ITrackTimeAccessor> m_time_accessor_pforv02 = nullptr;
//   std::unique_ptr<ITrackTimeAccessor> m_defaultpf_time_accessor = nullptr;
//   std::unique_ptr<ITrackTimeAccessor> m_truth_time_accessor = nullptr;//primes
//   std::unique_ptr<ITrackTimeAccessor> m_truth_time_accessor_tdrres = nullptr;//primes, 25ps
//   std::unique_ptr<TruthTrackTimeTunableAcc> m_tunable_truth_accessor;
//
//   std::vector<double> m_eff_rpt_cut;
//
//   TEfficiency* m_eff_hs_std_rpt= nullptr;
//   TEfficiency* m_eff_pu_std_rpt = nullptr;
//
//   const static int m_number_of_cutvals = 12;
//   double m_cutvaluestepsize = 0.5;
//   std::array<TEfficiency*, m_number_of_cutvals> m_eff_hs_subrpt_optim;
//   std::array<TEfficiency*, m_number_of_cutvals> m_eff_pu_subrpt_optim;
//
//
//   //efficiency plots needed for RpT and subjet RpT parametrisation
//   TH1F* m_bin_finder = nullptr;
//   const static int n_eta_bins = 10;
//   std::array<TEfficiency*, n_eta_bins> m_eff_hs_subjetrpt_param_low_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_hs_subjetrpt_param_high_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_pu_subjetrpt_param_low_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_pu_subjetrpt_param_high_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_hs_rpt_param_low_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_hs_rpt_param_high_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_pu_rpt_param_low_pt;
//   std::array<TEfficiency*, n_eta_bins> m_eff_pu_rpt_param_high_pt;
//
//   //comparing different track-jet association methods
//   // std::array<TEfficiency*, 4> m_eff_roc_ghost_track;
//   // std::array<TEfficiency*, 4> m_eff_roc_delta_r;
//   // TEfficiency* m_eff_hs_low_pt_ghost_track = nullptr;
//   // TEfficiency* m_eff_hs_high_pt_ghost_track = nullptr;
//   // TEfficiency* m_eff_pu_low_pt_ghost_track = nullptr;
//   // TEfficiency* m_eff_pu_high_pt_ghost_track = nullptr;
//   // TEfficiency* m_eff_hs_low_pt_delta_r = nullptr;
//   // TEfficiency* m_eff_hs_high_pt_delta_r = nullptr;
//   // TEfficiency* m_eff_pu_low_pt_delta_r = nullptr;
//   // TEfficiency* m_eff_pu_high_pt_delta_r = nullptr;
//
//
//   // rejection vs vertex density
//   // density from 0 to 6 vertices per mm, 0.5 steps
//   TH1F* m_denisty_bfh = nullptr; //bfh = bin finder histo
//   const static int n_density_bins = 16;
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_rpt_roc_density_bins; //ghost tracks
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_rpt_deltar0p4_roc_density_bins; //DeltaR 0.4 tracks
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_rpt_deltar0p3_roc_density_bins; //DeltaR 0.4 tracks
//
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_subjetrpt_density_bins;
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_subjetrpt_truthtime_density_bins;
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_subjetrpt_truthtime_perfect_density_bins;
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_hgtdrpt_defaultpf_density_bins;
//   std::array<std::array<TEfficiency*, 4>, n_density_bins> m_eff_hgtdrpt_tcv04pforv02_density_bins;
//
//   // rejection vs vertex eta
//   TH1F* m_eta_bfh = nullptr; //bfh = bin finder histo
//   const static int n_fulleta_bins = 20;
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_rpt_roc_eta_bins; //ghost tracks
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_rpt_deltar0p4_roc_eta_bins; //DeltaR 0.4 tracks
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_rpt_deltar0p3_roc_eta_bins; //DeltaR 0.4 tracks
//
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_subjetrpt_eta_bins;
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_subjetrpt_truthtime_eta_bins;
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_hgtdrpt_defaultpf_eta_bins;
//   std::array<std::array<TEfficiency*, 4>, n_fulleta_bins> m_eff_hgtdrpt_tcv04pforv02_eta_bins;
//
//
//   //////////////////////
//   // rpt curves
//   //////////////////////
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_truthtimeprimes;
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_truthtimeprimes_goodhsc;
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_truthtimeprimes_badhsc; // truth vx time, truth primary accessor
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_truthtimeprimes_goodhsctime_badhscitk;
//
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_pforv02;
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_cleanedpf;
//   std::array<TEfficiency*, 4> m_eff_rpt_truth_vx_time_pf;
//   std::array<TEfficiency*, 4> m_eff_rpt_std;
//   std::array<TEfficiency*, 4> m_eff_rpt_std_goodhsc;
//   std::array<TEfficiency*, 4> m_eff_rpt_std_badhsc;
//   std::array<TEfficiency*, 4> m_eff_subjetrpt_time_pforv02;
//   std::array<TEfficiency*, 4> m_eff_subjetrpt_time_truthtimeprimes;
//   std::array<TEfficiency*, 4> m_eff_subjetrpt_time_truthtimeprimes_tdrres;
//
//   std::array<TEfficiency*, 4> m_eff_rpt_default_reco_vx_time_pf;
//   std::array<TEfficiency*, 4> m_eff_rpt_default_reco_vx_time_cleanedpf;
//
//   const static int m_n_sigma_cuts = 10;
//   std::array<std::array<TEfficiency*, 4>, m_n_sigma_cuts> m_eff_rpt_default_reco_vx_time_pf_sigmacuts;
//
//   std::array<TEfficiency*, 4> m_eff_rpt_v01_reco_vx_time_pforv02;
//   std::array<TEfficiency*, 4> m_eff_rpt_v02_reco_vx_time_pforv02;
//   std::array<TEfficiency*, 4> m_eff_rpt_v02_reco_vx_time_truth_time;
//   std::array<TEfficiency*, 4> m_eff_rpt_v03_reco_vx_time_pforv02;
//   std::array<TEfficiency*, 4> m_eff_rpt_v04_reco_vx_time_pforv02;
//   std::array<TEfficiency*, 4> m_eff_rpt_v04_lowerpt_reco_vx_time_pforv02;
//   std::array<std::array<TEfficiency*, 4>, m_n_sigma_cuts> m_eff_rpt_v05_reco_vx_time_pforv02_sigmacuts;
//
//   //use scan over
//   //eff 100% - 50% in 5% steps
//   const static int m_n_eff_steps = 10;
//   //mistagging 0% to 50% in 5% steps
//   const static int m_n_mis_steps = 10;
//   //resolution from 5 ps to 80 ps in 5 ps steps
//   const static int m_n_res_steps = 15;
//   std::array<std::array<std::array<std::array<TEfficiency*, 4>, m_n_res_steps>, m_n_mis_steps>, m_n_eff_steps> m_eff_subjetrpt_scan;
//
//   //HS vertex time
//   TH1F* m_hist_vertex_time_res_default = nullptr;
//   TH1F* m_hist_vertex_time_has_time_default = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v01 = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v01 = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v02 = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v02 = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v02_truth = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v02_truth = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v03 = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v03 = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v04 = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v04 = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v05 = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v05 = nullptr;
//
//   TH1F* m_hist_vertex_time_res_v06 = nullptr;
//   TH1F* m_hist_vertex_time_has_time_v06 = nullptr;
//
//   TH1F* m_hist_std_rpt_hs = nullptr;
//   TH1F* m_hist_std_rpt_pu = nullptr;
//   TH1F* m_hist_default_hgtd_rpt_hs = nullptr;
//   TH1F* m_hist_default_hgtd_rpt_pu = nullptr;
//   TH1F* m_hist_v01_hgtd_rpt_hs = nullptr;
//   TH1F* m_hist_v01_hgtd_rpt_pu = nullptr;
//   TH1F* m_hist_v02_hgtd_rpt_hs = nullptr;
//   TH1F* m_hist_v02_hgtd_rpt_pu = nullptr;
//   TH1F* m_hist_v03_hgtd_rpt_hs = nullptr;
//   TH1F* m_hist_v03_hgtd_rpt_pu = nullptr;
//   TH1F* m_hist_v04_hgtd_rpt_hs = nullptr;
//   TH1F* m_hist_v04_hgtd_rpt_pu = nullptr;
//
//   //t0 from hs cluster
//
//   float getHSClusterTime(const std::vector<const xAOD::TrackParticle*>& tracks,
//     double cluster_distance, ITrackTimeAccessor* accessor, double efficiency, double purity);
//
//
//   std::vector<Cluster<const xAOD::TrackParticle*>>
//   clusterTracksInTime(
//     const std::vector<const xAOD::TrackParticle*>& tracks, double cluster_distance,
//     ITrackTimeAccessor* accessor);
//
//   float fractionHSInCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   TRandom3 m_rand;
//
//   TH1F* m_hist_vertex_time_res_hscluster = nullptr;
//
//   const static int n_workpoints = 26;
//   std::array<float, n_workpoints> m_hsc_eff{{0.2,  0.3,  0.4,  0.5,  0.6,   0.7,  0.8, 0.8,  0.8, 0.8,  0.8,  0.7, 0.7,  0.7, 0.7,  0.7,  0.6, 0.6,  0.6, 0.6,  0.6, 0.5, 0.5,  0.5, 0.5, 1.0}};
//   std::array<float, n_workpoints> m_hsc_pur {{0.98, 0.97, 0.96, 0.95, 0.925, 0.87, 0.8, 0.85, 0.9, 0.95, 0.98, 0.8, 0.85, 0.9, 0.95, 0.98, 0.8, 0.85, 0.9, 0.95, 0.98, 0.8, 0.85, 0.9, 0.98, 1.0}};
//   std::vector<bool> m_hsc_hastime = std::vector<bool>(n_workpoints, false);
//   std::vector<float> m_hsc_time = std::vector<float>(n_workpoints, -999.);
//
//   std::array<std::array<TEfficiency*, 4>, n_workpoints> m_eff_rpt_hscluster;
//   std::array<std::array<TEfficiency*, 4>, n_workpoints> m_eff_rpt_hscluster_truth_prime_time;
//   std::array<TH1F*, n_workpoints> m_hist_vertex_time_res_hscluster_scan;
//
//
//   std::unique_ptr<SG::AuxElement::Accessor<int > > m_dec_trackCategory;
//   std::unique_ptr<SG::AuxElement::Accessor<char > > m_dec_extensionCandidate;
//   std::unique_ptr<SG::AuxElement::Accessor<char > > m_dec_hasValidExtension;
//   std::unique_ptr<SG::AuxElement::Accessor<int > > m_dec_nHGTDLayers;
//   std::unique_ptr<SG::AuxElement::Accessor<int > > m_dec_nPossibleHGTDLayers;
//   std::unique_ptr<SG::AuxElement::Accessor<int > > m_dec_nMissedHGTDLayers;
//   std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_trueProdTime;
//   std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_meanTime;
//   std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_meanTimeChi2Weighted;
//   std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_expTimeFromZ0;
//   std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_rmsTime;
//   std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_RonLayer0;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<bool> > > m_dec_perLayer_hasCluster;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterChi2;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterR;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterRawTime;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterT0;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterDeltaT;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterX;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterY;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterZ;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<int> > > m_dec_perLayer_clusterTruthClassification;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<bool> > > m_dec_perLayer_clusterIsMerged;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<bool> > > m_dec_perLayer_clusterIsShadowing;
//   std::unique_ptr<SG::AuxElement::Accessor<std::vector<bool> > > m_dec_perLayer_expectCluster;
//
//   TTree* m_tree_events;
//   double m_tree_eventnumber;
//   double m_t0_perfect;
//   std::vector<double> m_t0_hsc;
//   int m_n_fwd_hs_jets;
//   int m_n_fwd_pu_jets;
//   int m_n_central_hs_jets;
//   int m_n_central_pu_jets;
//   int m_n_fwd_hs_tracks;
//   int m_n_fwd_hs_tracks_with_time;
//   int m_n_fwd_hs_tracks_with_gt50pcprime_time;
//
//   TTree* m_tree_jets;
//   int m_tree_jet_category;
//   double m_tree_jet_pt;
//   double m_tree_jet_eta;
//   double m_tree_jet_rpt;
//   double m_tree_jet_subjet_rpt;
//   double m_tree_jet_hgtd_rpt_trutht0;
//   std::vector<double> m_tree_jet_hgtd_rpt_hsc_trutht0;
//   std::vector<double> m_tree_jet_hgtd_rpt_hsc_wp_to;
//   double m_tree_jet_hgtd_rpt_t0_defaultreco;
//
//   //trees for marianna
//   // TTree* m_tree_hs;
//   // TTree* m_tree_pu;
//   //
//   // // double m_tree_eventnumber;
//   // // double m_tree_jet_pt;
//   // // double m_tree_jet_eta;
//   // // double m_tree_jet_rpt;
//   // // int m_tree_jet_category;
//   // double m_tree_density;
//   // double m_tree_n_central_jets;
//   // double m_tree_n_fwd_jets;
//   // double m_tree_toue;
//   // double m_tree_toue_pforv02;
//   // double m_tree_toue_pfdefault;
//   // double m_tree_toue_truthprimes;
//   // double m_tree_toue_truthtime_eff1; //100% efficiency, no impurity, smearing
//   // double m_tree_toue_truthtime_eff2;//67% efficiency, no impurity, smearing
//
// };
//
// #endif //> !HGTDANALYSISCORE_PILEUPREJECTION_H
