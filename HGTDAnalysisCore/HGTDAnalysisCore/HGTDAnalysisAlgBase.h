
/**
 * @Author Alexander Leopold (alexander.leopold@cern.ch)
 *
 * @brief  This algorithm is meant as a base class for HGTD studies, trying
 *  to provide a commong use of different functionalities in Athena.
 *
 *  To use it, create a new algorithm, inheriting from HGTDAnalysisAlgBase
 *  instead of the usual AthAlgorithm.
 *  (dont forget to #include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h")
 *
 * TODO:
 * - track vertex matching
 * - pileup density
 * - rpt
 */

#ifndef HGTDANALYSISCORE_ANALYSISALGBASE_H
#define HGTDANALYSISCORE_ANALYSISALGBASE_H 1
#ifndef XAOD_STANDALONE
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include "HGTDAnalysisCore/ClusteringHelper.h"
#include "HGTDTrackTimeInterface/ITrackTimeAccessor.h"

#include "TrkExInterfaces/IExtrapolator.h"
#include "TrkSurfaces/DiscSurface.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthVertex.h"

#include "MagFieldInterfaces/IMagFieldSvc.h"

#include "GaudiKernel/ServiceHandle.h"

#include "GaudiKernel/ITHistSvc.h"


#include "TVector3.h"

#include <vector>

class HGTDAnalysisAlgBase : public ::AthAlgorithm {

public:
  HGTDAnalysisAlgBase(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~HGTDAnalysisAlgBase();


  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();

  enum JetCategory { HS, QCD_PU, STOCH_PU, NEITHER_PU, NEITHER };

  /**
   * @brief  Matches a given reconstructed jet to truth jets, and classifies
   *  it as either hard scatter, qcd pile-up or stochastic pile-up jet. If a jet
   *  does not fall in any of the privious categories, it is classified as
   *  "NEITHER". For reference check:
   *  https://twiki.cern.ch/twiki/bin/view/AtlasSandboxProtected/TruthJetTaggingHFSAF15
   *
   * @param [in] jet Calibrated jet object.
   * @param [in] truth_jets Collection of truth jets of a given event.
   *
   * @return Gives the category the jet belogns to in terms of an enum.
   */
  JetCategory determineJetCategory(const xAOD::Jet* jet,
                                   const xAOD::JetContainer* truth_jets);

  /**
   * @brief Finds the tracks that can be associated to a given jet, using
   *  ghost track association.
   *
   * @param [in] jet Calibrated jet object.
   * @return The tracks that get clustered into the given jet.
   */
  std::vector<const xAOD::IParticle*> getGhostAssociatedTracks(xAOD::Jet* jet);

  std::vector<const xAOD::IParticle*>
  getGhostAssociatedTracks(const xAOD::Jet* jet);

  std::vector<const xAOD::TrackParticle*>
  getGhostAssociatedTrackParticles(const xAOD::Jet* jet);

  /**
   * @brief Finds the tracks that can be associated to a given jet, using
   *  a DeltaR cone of a given size.
   *
   * @param [in] jet Calibrated jet object.
   * @param [in] delta_r Maximum allowed DeltaR cone between jet a track.
   * @return The tracks that get clustered into the given jet.
   */
  std::vector<const xAOD::TrackParticle*>
  getDeltaRAssociatedTracks(xAOD::Jet* jet, double delta_r);

  std::vector<const xAOD::TrackParticle*>
  getDeltaRAssociatedTracks(xAOD::Jet* jet,
                            std::vector<const xAOD::TrackParticle*>& tracks,
                            double delta_r);

  int nGhostAssociatedTracks(xAOD::Jet* jet, const xAOD::Vertex* vx);

  // the standard implementation used ghost associated tracks
  double jetRpT(xAOD::Jet* jet, const xAOD::Vertex* vertex);
  double jetRpT(xAOD::Jet* jet, const xAOD::TruthVertex* vertex);

  double jetRpTHGTD(xAOD::Jet* jet, const xAOD::TruthVertex* vertex,
                    bool has_time, float vertex_time, float vertex_time_res,
                    ITrackTimeAccessor* accessor, float sigma_cut = 2.0);

  double jetRpTHGTD(xAOD::Jet* jet, const xAOD::Vertex* vertex, bool has_time,
                    float vertex_time, float vertex_time_res,
                    ITrackTimeAccessor* accessor, float sigma_cut = 2.0);

  // NOTE the jet has to be calibrated!!!
  // NOTE the track selection relative to the vertex has to have happend before
  // giving it to this method!!!
  double genericJetRpT(xAOD::Jet* jet,
                       std::vector<const xAOD::TrackParticle*>& tracks);

  double subJetRpT(xAOD::Jet* jet, const xAOD::Vertex* vertex,
                   ITrackTimeAccessor* accessor, double hit_time_resolution,
                   CH::ClusterAlgoType algo, double cluster_distance);

  double subJetRpT(xAOD::Jet* jet, const xAOD::TruthVertex* vertex,
                   ITrackTimeAccessor* accessor, double hit_time_resolution,
                   CH::ClusterAlgoType algo, double cluster_distance);

  /**
   * @brief Finds the TruthParticle the reconstructed track origins from, done
   *  via the dedicated element link that the track is decorated with. Returns
   *  nullptr if not found.
   */
  const xAOD::TruthParticle* getTruthParticle(const xAOD::TrackParticle* track);

  /**
   * @brief Finds the TruthVertex the reconstructed track origins from, which it
   *  does by first accessing the truth particle the track originates from, and
   *  then finding the vertex of that truth particle.
   *
   * @param [in] track A given reconstructed track.
   * @param [out] is_hs Can be used to check if the given track is hard scatter
   *  truth matched.
   * @return Vector of tracks that cluster into the given jet.
   */
  const xAOD::TruthVertex* getTruthVertex(const xAOD::TrackParticle* track,
                                          bool& is_hs);

  /**
   * @brief Since vertex time is stored in a rather particular way, we provide
   *  this function to get the correct value.
   */
  double vertexTruthTime(const xAOD::TruthVertex* truth_vertex);

  /**
   * @brief Returns the time of the vertex that the track can be associated to
   *  on truth level, doing so via the TruthParticle link. Returns a default
   *  value of -999 if the truth vertex is not found.
   */
  double trackTruthTime(const xAOD::TrackParticle* track, bool& is_hs);

  double parametrisedLocalVertexDensity(const xAOD::Vertex* vertex);

  // window size in mm
  double localVertexDensity(const xAOD::Vertex* vertex, double window);
  double localTruthVertexDensity(const xAOD::Vertex* vertex, double window);

  const xAOD::Vertex* getPrimaryRecoVertex();

  int nRecoVertices();

  const xAOD::TruthVertex* getTruthHSVertex();

  std::vector<const xAOD::TruthParticle*>
  getTruthParticles(const xAOD::TruthEvent* truth_event,
                    float min_pt_cut = 1000.);
  std::vector<const xAOD::TruthParticle*>
  getTruthParticles(const xAOD::TruthPileupEvent* truth_event,
                    float min_pt_cut = 1000.);

  /**
   * @brief Associates tracks to vertices using a parametrized version of the
   *  z0 resolution. Returns true if the track is compatible with the given
   *  vertex, false otherwise.
   * ATTENTION: THE IMPLEMENTED NUMBERS ARE FROM STEP3.0 AND NEED TO BE UPDATED
   * AT SOME POINT!!!
   */
  static bool passTrackVertexAssociation(const xAOD::TrackParticle* track,
                                         const xAOD::Vertex* vertex);

  static bool passTrackVertexAssociation(const xAOD::TrackParticle* track,
                                         const xAOD::Vertex* vertex,
                                         double min_trk_pt /*GeV*/);

  static bool passTrackVertexAssociation(const xAOD::TrackParticle* track,
                                         const xAOD::TruthVertex* vertex);

  std::vector<const xAOD::TrackParticle*>
  vertexAssociatedTracks(std::vector<const xAOD::TrackParticle*> tracks,
                         const xAOD::Vertex* vertex,
                         bool (*passes)(const xAOD::TrackParticle* track,
                                        const xAOD::Vertex* vertex));

  std::vector<const xAOD::TrackParticle*>
  vertexAssociatedTracks(const xAOD::Vertex* vertex,
                         bool (*passes)(const xAOD::TrackParticle* track,
                                        const xAOD::Vertex* vertex));

  std::vector<const xAOD::TrackParticle*>
  vertexAssociatedHGTDTracks(std::vector<const xAOD::TrackParticle*> tracks,
                             const xAOD::Vertex* vertex,
                             bool (*passes)(const xAOD::TrackParticle* track,
                                            const xAOD::Vertex* vertex));

  std::vector<const xAOD::TrackParticle*>
  vertexAssociatedHGTDTracks(const xAOD::Vertex* vertex,
                             bool (*passes)(const xAOD::TrackParticle* track,
                                            const xAOD::Vertex* vertex));

  std::vector<const xAOD::TrackParticle*>
  vertexAssociatedHGTDTracks(const xAOD::Vertex* vertex, double min_trk_pt);

  const xAOD::TruthEventContainer* getTruthEventContainer() {
    return m_truth_event_container;
  }

  /**
   * @brief Extrapolate a track from the last measurement on track to the
   * surfaces of HGTD.
   *
   * ATTENTION: before using this, you need to call
   * setupSurfacesForExtrapolation in your initialize function!
   *
   * @param [in] track A given reconstructed track.
   * @param [out] pos The position of the extrapolation on the chosen HGTD
   * layer.
   * @param [out] mom The direction of the momentum after the extrapolation on
   * the chosen HGTD layer.
   * @param [in] surface Selected surface of HGTD, has to be a value between 0
   * and 3;
   */
///////////////////////////////////////////////////////

 
 //properties
  std::string m_time_reco_alg;
  std::string m_directory_name;
        bool m_use_surfaces;
  bool m_use_pixelclusters;
  bool m_use_extrapolation;


  ServiceHandle<ITHistSvc> m_histSvc;


bool extrapolateTrackFromLastMeasurement(const xAOD::TrackParticle* track,
                                           TVector3& pos, TVector3& mom,
                                           int surface);

 
void setupSurfacesForExtrapolation();

  // distances of layers from the IP in mm, numbers for ATLAS-P2-ITK-17-04-02
  std::array<double, 5> m_hgtd_layer_z{
      {3443.8950, 3453.1050, 3468.8950, 3478.1050,3641}};

  std::array<Trk::DiscSurface*, 5> m_disc_positive_z;
  std::array<Trk::DiscSurface*, 5> m_disc_negative_z;

protected:
  ToolHandle<Trk::IExtrapolator> m_extrapolator;

        ServiceHandle<MagField::IMagFieldSvc>  m_magFieldSvc;
	
  // reco
  const xAOD::VertexContainer* m_primary_vertex_container = nullptr;

  // truth
  const xAOD::TruthPileupEventContainer* m_pileup_truth_container = nullptr;
  const xAOD::TruthEventContainer* m_truth_event_container = nullptr;

  const xAOD::TrackParticleContainer* m_track_container = nullptr;

  static constexpr double m_track_pt_cut =
      1000.; // min accepted track pt, in MeV!!!
  // static constexpr double m_track_pt_cut = 900.; //min accepted track pt, in
  // MeV!!!

  static constexpr double m_hit_resolution =
      0.035; // NOTE this has no meaning at the moment, and should be removed

private:



  std::vector<std::vector<TLorentzVector>> getTruthPuJets();

  bool includeParticle(const xAOD::TruthParticle* part);

  double m_n_time_sigma = 3.0;
};

#endif
#endif //> !HGTDANALYSISCORE_ANALYSISALGBASE_H
