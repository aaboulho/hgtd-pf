// #ifndef HGTDANALYSISCORE_TRACKTIMECONTROLPLOTS_H
// #define HGTDANALYSISCORE_TRACKTIMECONTROLPLOTS_H 1
//
// #include "AthenaBaseComps/AthAlgorithm.h"
// #include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
// #include "GaudiKernel/ITHistSvc.h"
// #include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"
//
// #include "HGTDTrackTimeInterface/AccessorFactory.h"
//
// #include "TEfficiency.h"
// #include "TProfile.h"
// #include "TH1F.h"
// #include "TH2D.h"
//
// class TrackTimeControlPlots: public ::HGTDAnalysisAlgBase {
//  public:
//   TrackTimeControlPlots( const std::string& name, ISvcLocator* pSvcLocator );
//   virtual ~TrackTimeControlPlots();
//
//   virtual StatusCode  initialize();
//   virtual StatusCode  execute();
//   virtual StatusCode  finalize();
//
//   ///// performance plots for different accessors
//   void recoPerformance(const xAOD::TrackParticle & track,
//                        ITrackTimeAccessor* accessor,
//                        int version);
//
//   void efficiencyPlotProposals(const xAOD::TrackParticle & track);
//
// 
//  private:
//
//   ServiceHandle<ITHistSvc> m_histSvc;
//
//   //define a "default" accessor
//   std::unique_ptr<ITrackTimeAccessor> m_time_accessor = nullptr;
//
//   std::unique_ptr<ITrackTimeAccessor> m_progfilt_acc = nullptr;
//   std::unique_ptr<ITrackTimeAccessor> m_segfind_acc = nullptr;
//
//   static const int m_n_categories = 2; //HS & PU
//   std::array<TH1F*, m_n_categories> m_hist_track_pt;
//   std::array<TH1F*, m_n_categories> m_hist_track_eta;
//   std::array<TH1F*, m_n_categories> m_hist_track_phi;
//   std::array<TH1F*, m_n_categories> m_hist_track_truth_time;
//
//   std::array<TEfficiency*, m_n_categories> m_eff_track_has_time;
//
//   std::array<TEfficiency*, m_n_categories> m_eff_track_has_time_in_window_vs_eta;
//   std::array<TEfficiency*, m_n_categories> m_eff_track_has_time_in_window_vs_pt;
//
//   TEfficiency* m_eff_track_has_good_time_vs_eta = nullptr;
//
//   std::array<TEfficiency*, 3> m_eff_track_good_reco_proposal_vs_eta;
//   std::array<TEfficiency*, 3> m_eff_track_good_reco_proposal_vs_phi;
//   std::array<std::array<TH1F*, 4>, 3> m_hist_time_resgood_reco_proposal_nhits;
//   //mistagging
//   std::array<TEfficiency*, 3> m_eff_track_bad_reco_proposal_vs_eta;
//   std::array<TEfficiency*, 3> m_eff_track_bad_reco_proposal_vs_phi;
//
//
//   std::array<std::array<TH1F*, 4>, m_n_categories> m_hist_time_res_nhits;
//
//   TH1F* m_hist_n_hs_tracks_in_hgtd = nullptr;
//   TH1F* m_hist_n_pu_tracks_in_hgtd = nullptr;
//
//   //// ----- recoPerformance plots -----
//   static const int n_versions = 2;//HS and PU
//   std::array<TEfficiency*, n_versions> m_eff_vs_eta;
//
//   std::array<TEfficiency*, n_versions> m_eff_vs_pt;
//
//   std::array<TProfile*, n_versions> m_prof_purity_vs_eta;
//
//   std::array<TH1F*, n_versions> m_hist_time_res;
//
//   std::array<TH1F*, n_versions> m_hist_n_assigned_hits;
//
//   std::array<std::array<TH1F*, 4>, n_versions> m_hist_time_res_n_hits;
//
//   double m_sensor_resolution;
//   std::string m_geometry_step;
//
//   //// ----- rates relative to truth particles
//   std::array<TEfficiency*, n_versions> m_truth_eff_vs_eta;
//
// };
//
// #endif //> !HGTDANALYSISCORE_TRACKTIMECONTROLPLOTS_H
