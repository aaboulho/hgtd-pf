#ifndef HGTDANALYSISCORE_HITCONTROLPLOTS_H
#define HGTDANALYSISCORE_HITCONTROLPLOTS_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ITHistSvc.h"

#include "GeneratorObjects/McEventCollection.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetIdentifier/SiliconID.h"
#include "InDetRawData/PixelRDO_Container.h"
#include "InDetReadoutGeometry/PixelDetectorManager.h"
#include "InDetReadoutGeometry/SCT_DetectorManager.h"
#include "InDetReadoutGeometry/SiDetectorElement.h"
#include "InDetRIO_OnTrack/PixelClusterOnTrack.h"
#include "InDetSimData/InDetSimDataCollection.h"
#include "TrkDetDescrUtils/BinUtility.h"
#include "TrkDetDescrUtils/BinnedArray1D1D.h"
#include "TrkGeometry/PlaneLayer.h"
#include "TrkSurfaces/RectangleBounds.h"
// #include "xAODTruth/TruthParticleContainer.h"
// #include "GeneratorObjects/McEventCollection.h"


#include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"

#include "TH1F.h"
#include "TEfficiency.h"
#include "TProfile.h"
#include "TH2F.h"

namespace InDetDD {
	class PixelDetectorManager;
	class SCT_DetectorManager;
}


class HitControlPlots: public ::HGTDAnalysisAlgBase {

 public:

  HitControlPlots( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~HitControlPlots();

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

	struct SdoInfo {
		float time;
		int truth; // signal=0, signal-seconary=1, pileup=2, delta=3
		bool operator<(const SdoInfo& rhs) const
    {
        return time < rhs.time;
    }
	};


  // void layerOccupancy(int layer, const std::vector<const Trk::PlaneLayer*> layers);
	void layerOccupancy();

 private:

  ServiceHandle<ITHistSvc> m_histSvc;

  const PixelID *m_pixel_ID;
  const SCT_ID *m_strips_ID;
  const InDetDD::PixelDetectorManager   *m_pixel_manager;
  const InDetDD::SCT_DetectorManager    *m_strips_manager;

	bool matchGenParticleToTruthParticle(const HepMC::GenParticle *gen_particle, int barcode, std::vector<const xAOD::TruthParticle*>& truth_particles);


  // int m_bins = 140;
 	// double m_min = -700.;
 	// double m_max = 700.;
  // int m_layer_offset = 5;
  // std::vector<const Trk::PlaneLayer*> m_layers_a;//4 layers
	// std::vector<const Trk::PlaneLayer*> m_layers_c;//4 layers
  // StatusCode populateSurfaceArrays();

  // const Trk::Surface* getIdentifiedSurface(const std::vector< const Trk::Surface* > array, Identifier id);


   //container has collection, that as the clusters
  const InDet::PixelClusterContainer* m_cluster_container;

	const PixelRDO_Container* m_pixel_rdo_container;

	const InDetSimDataCollection* m_sdo_collection;

	//position of primary vertices
	TH2D* m_primary_vx_radius_vs_z;
	TH2D* m_primary_vx_radius_vs_z_fine;
	TH1F* m_primary_vx_z;
	TH1F* m_primary_deltat_tof;

  //position of signal secondary vertices
	TH2D* m_secondary_vx_radius_vs_z;
	TH2D* m_secondary_vx_radius_vs_z_fine;
	TH1F* m_secondary_vx_z;
	TH1F* m_secondary_deltat_tof;
	TH2D* m_secondary_deltat_vs_vxz;

	//position of pile-up vertices
	TH2D* m_pileup_vx_radius_vs_z;
	TH2D* m_pileup_vx_x_vs_y;
	TH2D* m_pileup_vx_radius_vs_z_fine;
	TH1F* m_pileup_vx_z;
	TH1F* m_pileup_vx_radius;
	TH1F* m_pileup_deltat_tof;

  //for truth particles, how many leave primary hits in hgtd

  //for tracks, how many leave primary hits in hgtd

  //occupancy
  const static int m_layers = 4;
  std::array<TH2D*, m_layers> m_2dhist_hitspermodule_vs_radius;
  std::array<TProfile*, m_layers> m_prof_hitspermodule_vs_radius;

  std::array<TH2D*, m_layers> m_2dhist_occupancypermodule_vs_radius;
  std::array<TProfile*, m_layers> m_prof_occupancypermodule_vs_radius;

  TH1F* m_hist_n_hits_total = nullptr;

	TH1F* m_hist_toa_tof_primaries = nullptr;
	TH1F* m_hist_toa_tof_secondaries = nullptr;
	std::array<TH1F*, m_layers> m_hist_toa_tof_primaries_layer;
	std::array<TH1F*, m_layers> m_hist_toa_tof_secondaries_layer;
  //

};

#endif //> !HGTDANALYSISCORE_HITCONTROLPLOTS_H
