#ifndef HGTDANALYSISCORE_NTUPLEPRODUCTION_H
#define HGTDANALYSISCORE_NTUPLEPRODUCTION_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "GeneratorObjects/McEventCollection.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetPrepRawData/PixelCluster.h"
#include "InDetPrepRawData/PixelClusterContainer.h"
#include "InDetReadoutGeometry/PixelDetectorManager.h"
#include "InDetSimData/InDetSimDataCollection.h"
#include "JetCalibTools/JetCalibrationTool.h"

#include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"
#include "HGTDTrackTimeInterface/AccessorFactory.h"

#include "TTree.h"

namespace InDetDD {
class PixelDetectorManager;
class PixelCluster;
} // namespace InDetDD

class NtupleProduction : public ::HGTDAnalysisAlgBase {

  struct SdoInfo {
    float time;
    int truth; // signal=0, signal-seconary=1, pileup=2, delta=3
    bool operator<(const SdoInfo &rhs) const { return time < rhs.time; }
  };

public:
  NtupleProduction(const std::string &name, ISvcLocator *pSvcLocator);
  virtual ~NtupleProduction();

  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();

private:
  enum clusterTruthClassification {
    unmatched = 0,
    matchedToTrack = 1,
    matchedToHS = 2,
    matchedToPileUp = 3,
    matchedToSecondary = 4
  };

  // classifyCluster(const InDet::PixelCluster* clus, const xAOD::TruthParticle*
  // truthMatch, bool & isShadowingTrack, bool & isMerged);

  bool
  hitOriginatesFromTruthParticle(const InDet::PixelCluster *clus,
                                 const xAOD::TruthParticle *truth_particle);

  bool matchGenParticleToTruthParticle(
      const HepMC::GenParticle *gen_particle, int barcode,
      std::vector<const xAOD::TruthParticle *> &truth_particles);

  const PixelID *m_pixel_ID;
  const InDetDD::PixelDetectorManager *m_pixel_manager;
  // const SCT_ID *m_strips_ID;

  ServiceHandle<ITHistSvc> m_histSvc;

  // properties
  double m_jet_min_pt_cut;
  double m_track_min_pt_cut;
  bool m_include_hits;
  


  std::shared_ptr<ITrackTimeAccessor> m_time_accessor = nullptr;
  // std::shared_ptr<ITrackTimeAccessor> m_truth_time_accessor = nullptr;

  ToolHandle<IJetCalibrationTool> m_jet_calibration_tool;
  // ToolHandle<IAthSelectionTool> m_truthSelectionTool;

  const InDetSimDataCollection *m_sdo_collection;


  // per-jet level information
  TTree *m_jet_tree = nullptr;

  float m_jet_px; // in GeV
  float m_jet_py; // in GeV
  float m_jet_pz; // in GeV
  float m_jet_e;  // in GeV
  float m_jet_pt; // in GeV
  float m_jet_eta;
  float m_jet_phi;
  float m_jet_mass; // in GeV
  int m_jet_class;  // HS=0, QCD_PU=1, STOCH_PU=2, PU=3;
  float m_jet_pt_corr; // in GeV
  float m_HStrack_sumpt;
  int m_nmb_PUtrack=0;
  int m_nmb_HStrack=0;
  int m_nmb_track=0;
  int m_nmb_TruthP=0;
  int m_nmb_TruthPHS=0;
  int m_nmb_TruthPPU=0;

float m_DeltaRPV;
float m_DeltaREM1;
float m_tree_density;
float m_DeltaR;
float m_magFieldSvc1;
float m_magFieldSvc2;

  float m_truth_jet_e;
  float m_truth_jet_eta;
  float m_truth_jet_pt;
  float m_truth_jet_phi;
  TH1D* m_JetHist =0;
  float tracks;

  // per-track level information
//  TTree *m_track_tree = nullptr;

  std::vector<float> m_track_pt; // in GeV
  float m_track_sump;
  float m_track_sumpt;
  float delta ;
//  float m_tracksumpt;

 
  std::vector<float> m_HStrack_pt;
  std::vector<float> m_track_eta;
  std::vector<float> m_track_phi;
  std::vector<float> m_track_p;
  std::vector<float> vertex_time;
  //std::vector<float> m_track_z0;
  float m_track_type; // HS=0, PU=1, fake=2
  std::vector<float> track_time;
  std::vector<float> delta_t;
  bool m_track_reco_time_exists;
  float m_track_reco_time; // reconstructed time
  float m_track_reco_time_nhits;
  std::vector<float> m_track_reco_time_hit_x;
  std::vector<float> m_track_reco_time_hit_y;
  std::vector<float> m_track_reco_time_hit_z;


  bool m_track_vx_truth_time_exists;
  float m_track_vx_truth_time; // time of the truth associated vertex
  float m_track_vx_truth_z;
  bool m_track_truth_time_exists;
  float m_track_truth_time; // smeared time using truth information
  float m_track_truth_time_nhits;

  // per-hit level information
  TTree *m_hit_tree = nullptr;

  unsigned int m_hit_module_id;
  int m_hit_module_layer;    // 0, 1, 2, 3
  float m_hit_module_x;      // in mm
  float m_hit_module_y;      // in mm
  float m_hit_module_z;      // in mm
  float m_hit_module_radius; // in mm
  float m_hit_x;             // in mm
  float m_hit_y;             // in mm
  float m_hit_z;             // in mm
  float m_hit_raw_time;      // in ns
  bool m_hit_is_primary_from_hs;
  bool m_hit_is_primary;

  TTree *m_rdo_hit_tree = nullptr;
  int m_rdo_hit_module_layer;  // 0, 1, 2, 3
  float m_rdo_hit_module_id;   // in mm
  float m_rdo_hit_module_x;    // in mm
  float m_rdo_hit_module_y;    // in mm
  float m_rdo_hit_module_z;    // in mm
  float m_rdo_hit_x;           // in mm
  float m_rdo_hit_y;           // in mm
  float m_rdo_hit_z;           // in mm
  float m_rdo_hit_raw_time;    // in ns
  float m_rdo_hit_sdoraw_time; // in ns
  int m_rdo_hit_bcid;
  int m_rdo_hit_truth; // signal=0, signal-secondary=1, pileup=2, delta=3

  TTree *m_truth_vx_tree = nullptr;

  bool m_truth_vx_is_hs;
  float m_truth_vx_x;
  float m_truth_vx_y;
  float m_truth_vx_z;
  float m_truth_vx_time;

  TTree *m_reco_vx_tree = nullptr;

  bool m_reco_vx_is_hardest;
  float m_reco_vx_x;
  float m_reco_vx_y;
  float m_reco_vx_z;

  // per-event level information
  int m_run_number;
  int m_event_number;
  int m_event_bcid;
};

#endif //> !HGTDANALYSISCORE_NTUPLEPRODUCTION_H
