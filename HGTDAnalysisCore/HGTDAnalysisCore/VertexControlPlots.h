// #ifndef HGTDANALYSISCORE_VERTEXCONTROLPLOTS_H
// #define HGTDANALYSISCORE_VERTEXCONTROLPLOTS_H 1
//
// #include "AthenaBaseComps/AthAlgorithm.h"
// #include "GaudiKernel/ITHistSvc.h"
// #include "GaudiKernel/ToolHandle.h"
// #include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"
//
// #include "HGTDAnalysisCore/ClusteringHelper.h"
// #include "HGTDTrackTimeInterface/AccessorFactory.h"
//
// #include <vector>
// 
// #include "TEfficiency.h"
// #include "TProfile.h"
// #include "TH1F.h"
// #include "TH2D.h"
// #include "TTree.h"
//
// class VertexControlPlots: public ::HGTDAnalysisAlgBase {
//  public:
//   VertexControlPlots( const std::string& name, ISvcLocator* pSvcLocator );
//   virtual ~VertexControlPlots();
//
//   virtual StatusCode  initialize();
//   virtual StatusCode  execute();
//   virtual StatusCode  finalize();
//
//   std::vector<Cluster<const xAOD::TrackParticle*>>
//   clusterTracksInTime(
//     const std::vector<const xAOD::TrackParticle*>& tracks, double cluster_distance,
//     ITrackTimeAccessor* accessor);
//
//   float fractionHSInCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   std::pair<float, float> getZOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   std::pair<float, float> getDOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   std::pair<float, float> getOneOverPOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   float getSumPt2OfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);
//
//   bool passSigmaBasedTrackVxAssoc(const xAOD::TrackParticle* track,
//     const xAOD::Vertex* vertex, float distance_cut);
//
//  private:
//
//   ServiceHandle<ITHistSvc> m_histSvc;
//
//   std::shared_ptr<ITrackTimeAccessor> m_time_accessor;
//
//   std::string m_directory_name;
//
//   float m_max_vertex_deltaz = 2.0;
//   float m_z0_distance_cut = 2.0;
//
//   float m_cluster_distance = 3.0;
//
//   float m_track_min_eta = 2.4;
//   float m_track_max_eta = 4.0;
//
//   //check the other vertices
//   TH1F* m_hist_n_closeby_vertices = nullptr;
//
//   //some diagnostics of track variables
//   TH1F* m_hist_track_z0res = nullptr;
//
//   //what is happening in track/vx association
//   TH1F* m_hist_n_selected_tracks = nullptr;
//   TH1F* m_hist_n_selected_tracks_with_time = nullptr;
//   TH1F* m_hist_n_selected_hs_tracks = nullptr;
//   TH1F* m_hist_n_selected_hs_tracks_with_time = nullptr;
//   TH1F* m_hist_fraction_hs_tracks = nullptr;
//   TH1F* m_hist_fraction_hs_tracks_withtime = nullptr;
//
//   //what do the clusters look like
//   TH1F* m_hist_n_clusters = nullptr;
//   TH1F* m_hist_n_hs_clusters = nullptr;
//   TH1F* m_hist_timediff_hs_clusters = nullptr;
//
//   TH1F* m_hist_z0_res_hs_cluster = nullptr;
//   TH1F* m_hist_z0_res_pu_cluster = nullptr;
//
//   TTree* m_signal_tree = nullptr;
//   TTree* m_background_tree = nullptr;
//
//   float m_hs_fraction;
//   int m_is_hs_cluster;
//   float m_delta_t;
//   float m_delta_z;
//   float m_z_sigma;
//   float m_q_over_p;
//   float m_q_over_p_sigma;
//   float m_d0;
//   float m_d0_sigma;
//   float m_delta_z_resunits;
//   float m_cluster_sumpt2;
//   int m_number_of_tracks;
//
//
// };
//
// #endif //> !HGTDANALYSISCORE_VERTEXCONTROLPLOTS_H
