#ifndef HGTDANALYSISCORE_PILEUPREJECTIONNTUPLE_H
#define HGTDANALYSISCORE_PILEUPREJECTIONNTUPLE_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "HGTDAnalysisCore/ClusteringHelper.h"
#include "HGTDAnalysisCore/HGTDAnalysisAlgBase.h"
#include "HGTDTrackTimeInterface/AccessorFactory.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

#include <vector>

#include "TEfficiency.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TProfile.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TVector2.h"

class PileupRejectionNtuple : public ::HGTDAnalysisAlgBase {

  enum EventType { HS, PU, FAKE };

  struct MatchedTrack {
    const xAOD::TrackParticle* reco_trk;
    const xAOD::TruthParticle* true_trk;
    int event_type; // HS PU FAKE
    float track_weight = 0.0;
    ElementLink<xAOD::TruthEventBaseContainer> event;
  };

  struct VertexTimeCluster {
    std::vector<float> tracktimes;
    int ntracks = 0;
    std::vector<MatchedTrack> tracks;
    float time = 0.0;
  };

  std::vector<MatchedTrack> collectMatchedTracks(const xAOD::Vertex* vertex);

  bool
  isHardScatterEvent(const ElementLink<xAOD::TruthEventBaseContainer>& evlink);

  float defaultVxTimeWithAccessor(const std::vector<MatchedTrack>& tracks,
                                  ITrackTimeAccessor* accessor);

  float getVertexTime(std::vector<VertexTimeCluster> clusters,
                      std::string sortingType = "sumpt2" /*, int ibc*/);

  float getTrackProperties(VertexTimeCluster cluster, std::string prop);

  float calculateClusterTime(VertexTimeCluster cluster);

public:
  PileupRejectionNtuple(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~PileupRejectionNtuple();

  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();

private:
  std::string m_directory_name = "";

  double m_max_vertex_deltaz = 2.0;
  double m_jet_pt_low_cut = 30.;
  double m_jet_pt_high_cut = 50.;
  double m_jet_pt_max_cut = 80.;
  std::string m_used_accessor = "";
  std::string m_used_purej_accessor = "";

  ServiceHandle<ITHistSvc> m_histSvc;
  ToolHandle<IJetCalibrationTool> m_jet_calibration_tool;

  AsgForwardElectronIsEMSelector* m_fwd_electron_selector;

  std::shared_ptr<ITrackTimeAccessor> m_time_accessor =
      nullptr; // most up to date
  std::shared_ptr<ITrackTimeAccessor> m_time_accessor_pu_rejection =
      nullptr; // most up to date

  // std::shared_ptr<ITrackTimeAccessor> m_time_accessor_pf = nullptr;
  // std::shared_ptr<ITrackTimeAccessor> m_time_accessor_pforv02 = nullptr;
  // std::unique_ptr<ITrackTimeAccessor> m_defaultpf_time_accessor = nullptr;
  // std::unique_ptr<ITrackTimeAccessor> m_truth_time_accessor = nullptr; //
  // primes std::unique_ptr<ITrackTimeAccessor> m_truth_time_accessor_tdrres =
  //     nullptr; // primes, 25ps
  // std::unique_ptr<TruthTrackTimeTunableAcc> m_tunable_truth_accessor;

  // t0 from hs cluster

  float getHSClusterTime(const std::vector<const xAOD::TrackParticle*>& tracks,
                         double cluster_distance, ITrackTimeAccessor* accessor,
                         double efficiency, double purity);

  std::vector<Cluster<const xAOD::TrackParticle*>>
  clusterTracksInTime(const std::vector<const xAOD::TrackParticle*>& tracks,
                      double cluster_distance, ITrackTimeAccessor* accessor);

  float fractionHSInCluster(const Cluster<const xAOD::TrackParticle*>& cluster);

  std::pair<float, float>
  getZOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);

  std::pair<float, float>
  getDOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);

  std::pair<float, float>
  getOneOverPOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);

  std::pair<float, float>
  getPropertyOfCluster(const Cluster<const xAOD::TrackParticle*>& cluster,
                       float (xAOD::TrackParticle::*prop)() const, int index);

  float getSumPt2OfCluster(const Cluster<const xAOD::TrackParticle*>& cluster);

  float getLogProduct(const Cluster<const xAOD::TrackParticle*>& cluster,
                      int index);

  int closestVertexIsPrimaryVertex(float cluster_z,
                                   const xAOD::VertexContainer* reco_vertices,
                                   const xAOD::Vertex* primary_vx);

  float distanceClosestPUVertex(float cluster_z,
                                const xAOD::VertexContainer* reco_vertices,
                                const xAOD::Vertex* primary_vx);

  int nPUVerticesInVicinity(float cluster_z,
                            const xAOD::VertexContainer* reco_vertices,
                            const xAOD::Vertex* primary_vx, float delta = 1.0);

  float
  twoDimensionalPtVectorSum(const Cluster<const xAOD::TrackParticle*>& cluster);

  // 0 = d0, 1 = z0, 4 = q/p
  float getPropertyResolutionVariance(
      const Cluster<const xAOD::TrackParticle*>& cluster, const int index);

  float
  getPropertyResolutionMean(const Cluster<const xAOD::TrackParticle*>& cluster,
                            const int index);

  // remove jets overlapping with leptons
  void l_j_overlap(std::vector<const xAOD::IParticle*>& leptons,
                   std::vector<xAOD::Jet*>& jets, double dRmin);

  void tau_jet_overlap(std::vector<const xAOD::IParticle*>& taus,
                       std::vector<xAOD::Jet*>& jets, double dRmin);

  void l_track_overlap(std::vector<const xAOD::IParticle*>& leptons,
                       std::vector<const xAOD::TrackParticle*>& tracks,
                       double dRmin);
  // event selection

  enum CUTFLOW {
    TwoRpTJets = 2,
    LeadingPt = 3,
    SubleadingPt = 4,
    DeltaEta = 5,
    GoodEvent = 6 // one more to have "pass all" entry in cutflow
  };

  CUTFLOW
  eventPassesVBFSelection(const std::vector<xAOD::Jet*>& pt_ordered_jets);

  enum CUTFLOW2 {
    AtLeastTwoJets = 2, // both have PT>30
    DeltaEtaJets = 3,   // delta eta between leading and subleading
    LeadingRpT = 4,
    SubleadingRpT = 5,
    LeadingRpT2 = 6,
    SubleadingRpT2 = 7,
    PassEvent = 8 // one more to have "pass all" entry in cutflow
  };

  CUTFLOW2
  eventPassesVBFSelection2(const std::vector<xAOD::Jet*>& pt_ordered_jets,
                           const xAOD::Vertex* reco_vx);

  enum JETSELCUT {
    NoJet = 0,
    Cat1 = 1,
    Cat2 = 2,
    Cat3 = 3,
    Cat4 = 4,
    None = 5
  };

  JETSELCUT
  eventSelectionOneJet(const std::vector<xAOD::Jet*>& pt_ordered_jets,
                       const xAOD::Vertex* reco_vx);

  TRandom3 m_rand;

  TH1F* m_hist_cutflow;

  TTree* m_tree_jets;

  int m_tree_eventnumber;
  double m_tree_event_weight;
  bool m_tree_event_passesvbf;
  double m_tree_truth_t0;
  double m_tree_param_vx_density;
  double m_tree_local_vx_density;
  double m_tree_local_vx_density_1p0;
  double m_tree_local_vx_density_0p5;
  double m_tree_local_truthvx_density;
  double m_tree_deltaz_vx_truthvx;

  int m_tree_n_hgtd_tracks;
  int m_tree_n_hgtd_tracks_w_time;
  int m_tree_n_hgtd_hs_tracks;
  int m_tree_n_hgtd_hs_tracks_with_time;
  int m_tree_n_hgtd_hs_tracks_with_good_time;
  // clusters
  std::vector<double> m_tree_cluster_time;
  std::vector<double> m_tree_cluster_time_res;
  std::vector<double> m_tree_cluster_z0;
  std::vector<double> m_tree_cluster_z0_sigma;
  std::vector<double> m_tree_cluster_z0_stddev;
  std::vector<double> m_tree_cluster_z0_mean;
  std::vector<double> m_tree_cluster_q_over_p;
  std::vector<double> m_tree_cluster_q_over_p_sigma;
  std::vector<double> m_tree_cluster_d0;
  std::vector<double> m_tree_cluster_d0_sigma;
  std::vector<double> m_tree_cluster_hs_fraction;
  std::vector<double> m_tree_cluster_delta_z;
  std::vector<double> m_tree_cluster_delta_z_resunit;
  std::vector<double> m_tree_cluster_sumpt2;
  std::vector<double> m_tree_cluster_ntracks;
  std::vector<double> m_tree_cluster_closest_vx_is_pvx; // 0==no, 1==yes
  std::vector<double> m_tree_cluster_distance_pu_vx;
  std::vector<double> m_tree_cluster_n_pu_vertices;
  std::vector<double> m_tree_cluster_pt_mod;

  int m_tree_jet_category;
  double m_tree_jet_pt;
  double m_tree_jet_eta;
  double m_tree_jet_rpt;
  double m_tree_jet_hgtd_rpt_trutht0_truth_time;
  double m_tree_jet_hgtd_rpt_trutht0_reco_time;
  double m_tree_jet_hgtd_rpt_smearedt0_truth_time;
  double m_tree_jet_hgtd_rpt_smearedt0_reco_time;
  double m_tree_jet_subjet_rpt;
  std::vector<double> m_tree_jet_hgtd_rpt_cluster_times;
  std::vector<double> m_tree_jet_hgtd_rpt_cluster_times_truth_trk;
  // double m_tree_jet_hgtd_rpt_t0_defaultreco;
};

#endif //> !HGTDANALYSISCORE_PILEUPREJECTION_H
