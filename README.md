# HGTDAnalysisCore

This ATHENA project is meant to facilitate full simulation studies for HGTD, allowing for homogeneous usage of timing information.

## Getting started
Here is how to retrieve the setup and get started by running a short example (tested on lxplus). As you can see wee need to checkout the correct version of the jet calibration tool (see [here](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ApplyJetCalibration2016#Calibration_of_the_AntiKt4EMTopo)).

### Basic setup and retrieval of repository

```
git clone --recursive https://:@gitlab.cern.ch:8443/aleopold/hgtdanalysiscore.git
cd hgtdanalysiscore
git submodule init
git submodule update --init
setupATLAS
asetup 20.20.14.6,here
unset TCMALLOCDIR
svn co https://svn-atlas.web.cern.ch/repo/Reconstruction/Jet/JetCalibTools/tags/JetCalibTools-00-04-80 Reconstruction/Jet/JetCalibTools
echo "JetCalibTools-00-00-00" >> Reconstruction/Jet/JetCalibTools/cmt/version.cmt 
setupWorkArea.py
cd WorkArea/cmt
cmt find_packages
cmt bro
cmt config
cmt compile
cd $TestArea
```

After each login, do:
```
setupATLAS
asetup 20.20.14.6,here
unset TCMALLOCDIR
```
It is quite handy to have this in a script and reuse, which is why I left a `setup.sh` in the repository for your convenience.

### Running a job

For local running, and to keep the repository clean, it is suggested to run from within a `run` folder:
```
cd $TestArea
mkdir run
cd run
athena ../HGTDAnalysisCore/share/JetControlPlots_JO.py | tee logfile
```

### Submitting to the grid

Due to compatibility issues between CentOS7 and the allocator library used for Release 20.20, it is recommended to submit to grid from slc6 machines. To do this, you need to login to an slc6 node, recompile your local package clone and submit to grid from there

```
# login, NOTE THE 6!!
ssh -Y <username>@lxplus6.cern.ch
# ...
# go to the repo and do the setup as usual
# ...
cd $TestArea/WorkArea/cmt; cmt bro make clean; cmt find_packages; cmt compile
cd $TestArea/run

# now setup panda
lsetup panda
pathena <path/to/JO> --excludedSite "ANALY_BNL*" --inDS <name.of.input.container>  --outDS <user.name.of.output.container>
```

## Usage of the time information

The reconstructed track times are added to the `TrackParticle` as decorations. To have a uniform usage of this information, we are providing accessor interfaces in the `HGTDTrackTimeInterface` package.
Since we have different reconstruction algorithms and will be able to define different working points in terms of efficiency and purity of the track-time matching, different classes will be developed for this.
To avoid having to update all of your analysis code any time a new class was defined, it is recommended to use the accessors via the `ITrackTimeAccessor` interface class that all accessors inherit from and which defines the basic functionality.

#### Example usage:
In the header file of your algorithm, include (using the default progressive filter accesor as an example)
```
#include "HGTDTrackTimeInterface/TrackTimeProgFiltAcc.h"
```

and declare a private member
```
std::unique_ptr<ITrackTimeAccessor> m_accessor = nullptr;
```

initialize the accessor by adding the line
```
m_accessor = std::make_unique<TrackTimeProgFiltAcc>();
```
in the `initialize()` method of your algorithm.

To access the reconstructed time, in the `execute()` method do:
```
const xAOD::TrackParticleContainer* track_container = nullptr;
if ( not evtStore()->retrieve(track_container,"InDetTrackParticles").isSuccess() ) {
  ATH_MSG_ERROR ("Failed to retrieve InDetTrackParticles collection. ");
  return StatusCode::SUCCESS;
}

for (const auto& track : *track_container) {
  if (accessor->hasTimeAssigned(*track)) {
    float track_time = accessor->getTime(*track);
    //do stuff...
  }
}
```

Once a new accessor comes around that you want to try out, the only points that need change are
```
//#include "HGTDTrackTimeInterface/TrackTimeProgFiltAcc.h"
#include "HGTDTrackTimeInterface/NewFancyAcc.h"
```
```
//m_accessor = std::make_unique<TrackTimeProgFiltAcc>();
m_accessor = std::make_unique<NewFancyAcc>();
```
and the rest of your code will continue to work.

## The `HGTDAnalysisAlgBase` algorithm

To centrally provide often used functionality, this class can be inherited from and the available methods used in your Athena algorithm. The implemented methods are in detail described in the `HGTDAnalysisAlgBase.h` header, with doxygen strings added to each method that was deemed not completely self explanatory.
In case you are missing functionality that you think can be useful, or find a bug, please don't hesitate to open an issue on gitlab.

## Workflow

### Create a new algorithm

I would recommend to use the functionality provided by cmt

```
cmt new_alg MyAlgorithmName
```

If you want to inherit from `HGTDAnalysisAlgBase`, make sure to change `AthAlgorithm` to `HGTDAnalysisAlgBase` in your code.
You also need to add the corresponding `initialize`, `execute` and `finalize` methods of the base algorithm in your source code.
```
StatusCode MyAlgorithmName::initialize() {
  ATH_CHECK(HGTDAnalysisAlgBase::initialize());
  //set up stuff...
}

StatusCode MyAlgorithmName::execute() {
  ATH_CHECK(HGTDAnalysisAlgBase::execute());
  //run over the events ...
}

StatusCode MyAlgorithmName::finalize() {
  ATH_CHECK(HGTDAnalysisAlgBase::finalize());
  //finalize stuff...
}
```

## Updating the track time accessors

The `HGTDTrackTimeInterface` submodule will continuously be further developed. So you might be asked from time to time to update the submodule in your local repository, which will allow you access to the latest and greatest. For that do:
```
git submodule update --remote
```
